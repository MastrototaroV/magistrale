
% Module: questions.pl
% --------
%
%Il modulo contiene tutte le domande che serviranno ad interagire con l'utente, 
%suddivise in due sottogruppi: domande di carattere generale e domande specifiche

:- module(questions,[chiedi/2]).

%Domande di carattere generale
chiedi(frequenza_utilizzo,'Con quale frequenza hai utilizzato il sistema negli ultimi 60 giorni?').
chiedi(durata_nel_tempo,'Il problema si sta manifestando in maniera intermittente o prolungata?'). 
chiedi(minuti,'Da quanti minuti rilevi il problema?').
chiedi(piu_applicazioni_coinvolte,'La problematica si manifesta in modo analogo su altri sistemi?').
chiedi(piu_utenti_coinvolti,'La problematica è riscontrata anche da altri utenti?').
chiedi(problemi_analoghi,'Hai riscontrato problemi analoghi in passato?').
chiedi(frequenza_problemi_analoghi,'Quante volte hai riscontrato il problema?').

%Domande relative alla fase di autenticazione
chiedi(mancata_autenticazione,'Riscontri difficoltà durante la fase di login?').
chiedi(registrazione_al_sistema,'In passato sono stati già effettuato altri accessi al sistema?').
chiedi(rimane_su_pagina_iniziale,'L\'operazione di inserimento delle credenziali riporta l\'utente sulla pagina iniziale?').
chiedi(tentativi_login,'Inserire il numero di prove effettuate: ').
chiedi(disconnessione_login,'Si ricevono messaggi di errore dopo il tentativo di autenticazione?').
chiedi(dispositivi,'L\'utente possiede strumenti personali di accesso al sistema?').
chiedi(connettivita,"Si riscontra la mancata visibilità della pagina di accesso iniziale?").
chiedi(err_code,'Indicare il codice di errore (codici disponibili: 400, 401, 403, 404, 500, 502, 503):').
chiedi(non_affidabile,'La pagina viene contrassegnata come "non affidabile"?').
chiedi(malware,'Si visualizzano informazioni indesiderate all\'interno del sistema?').
chiedi(browser,'Si riscontrano problemi analoghi utilizzando altri browser?').
chiedi(lentezza_server,'Si rileva lentezza durante l\'utilizzo del sistema?').
chiedi(lentezza_rete,'Si rileva lentezza durante l\'utilizzo del sistema?').
chiedi(lentezza_pc,'Si rileva lentezza durante l\'utilizzo del sistema?').
chiedi(blocco,'Si è impossibilitati ad eseguire comandi sul sistema?').

%%Domande relative alle funzionalità
chiedi(finalita,'Con quali finalità si intende utilizzare il sistema?').
chiedi(funzioni,'L\'applicazione visualizza correttamente tutte le funzionalità a disposizione?').
chiedi(disconnessione_utilizzo,'Si rilevano disconnessioni impreviste durante l\'utilizzo del sistema?').
chiedi(visibilita,'Le directory contengono tutti i documenti richiesti?').
chiedi(doc,'L\'utente riesce ad operare correttamente sui documenti?').
chiedi(transfer,'L\'utente riesce ad importare/esportare correttamente tutti i documenti?').
chiedi(stampa,'Si riscontrano difficoltà durante le operazioni di stampa?').
chiedi(info_scorrette,'Le ricerche restituiscono informazioni diverse da quelle desiderate?').
chiedi(info_mancanti,'Le ricerche restituiscono informazioni incomplete o mancanti?').
chiedi(posta,'Si riscontrano difficoltà con l\'invio-ricezione di email?').
chiedi(allegati,'Si riscontrano difficoltà con l\'utilizzo degli allegati?').
chiedi(schedulazioni,'Si rilevano difficoltà legate ad attività giornaliere pianificate?').
chiedi(orario,'L\'orario dell\'applicazione risulta correttamente sincronizzato con il proprio PC?').