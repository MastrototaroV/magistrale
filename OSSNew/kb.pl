
:- dynamic fact/1.

asserzioni([goal(init)]).

%frequenza accesso
rule 1:
[goal(init)]
==>
[nl,
 write('DOMANDE DI CARATTERE GENERALE'),
 assert(certezza(0)),
 question_id(ID),
 domanda(ID,frequenza_utilizzo,VAL,frequenza),
 assert(risposta(ID,frequenza,problem(frequenza_utilizzo,VAL)))]
explained_by []
severity 2.

		%problemi analoghi in passato
		rule 2:
		[goal(init)]
		==>
		[question_id(ID),
		 domanda(ID,problemi_analoghi,VAL,incert),
		 assert(risposta(ID,incert,problem(problemi_analoghi,VAL)))]
		explained_by []
		severity 2.
		
				%frequenza problemi analoghi in passato
				rule 3:
				[goal(init),
				 risposta(_,_,problem(frequenza_utilizzo,alta)),
				 risposta(_,_,problem(problemi_analoghi,CERT)), CERT > 0.5]
				==>
				[question_id(ID),
				 domanda(ID,frequenza_problemi_analoghi,VAL,numeric),
				 assert(risposta(ID,numeric,problem(frequenza_problemi_analoghi,VAL)))]
				explained_by [3]
				severity 2.

%ricorrenza
rule 4:
[goal(init)]
==>
[question_id(ID),
 domanda(ID,durata_nel_tempo,VAL,ricorrenza),
 assert(risposta(ID,ricorrenza,problem(durata_nel_tempo,VAL)))]
explained_by []
severity 2.

		%minuti
		rule 5:
		[goal(init),
		 risposta(_,_,problem(problemi_analoghi,CERT)), CERT < 0.5,
		 risposta(_,_,problem(durata_nel_tempo,prolungata))]
		==>
		[question_id(ID),
		 domanda(ID,minuti,VAL,numeric),
		 assert(risposta(ID,numeric,problem(minuti,VAL)))]
		explained_by [5]
		severity 2.

%piu utenti
rule 6:
[goal(init)]
==>
[question_id(ID),
 domanda(ID,piu_utenti_coinvolti,VAL,incert),
 assert(risposta(ID,incert,problem(piu_utenti_coinvolti,VAL)))]
explained_by []
severity 3.

%piu applicazioni
rule 7:
[goal(init)]
==>
[question_id(ID),
 domanda(ID,piu_applicazioni_coinvolte,VAL,incert),
 assert(risposta(ID,incert,problem(piu_applicazioni_coinvolte,VAL)))]
explained_by []
severity 3.


%AUTENTICAZIONE
rule 8:
[goal(init)]
==>
[nl,
 write('DOMANDE SPECIFICHE'),
 retract(goal(init)),
 question_id(ID),
 domanda(ID,mancata_autenticazione,VAL,incert),
 assert(risposta(ID,incert,problem(mancata_autenticazione,VAL)))]
explained_by []
severity 2.




rule 11:
[risposta(_,_,problem(mancata_autenticazione,CERT1)), CERT1 > 0.5,
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
==>
[question_id(ID),
 domanda(ID,registrazione_al_sistema,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT1,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(mancata_autenticazione,CERT1))),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(registrazione_al_sistema,VAL)))]
 explained_by [11]
 severity 1.

	rule 111:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	 risposta(_,_,problem(registrazione_al_sistema,CERT4)), CERT4 < 0.5,
	 risposta(_,_,problem(frequenza_utilizzo,nonso))]
	==>
	[assert(incident(security)),
	 assert(dettaglio(user)),
	 assert(soluzione(creazione)),
	 assert(goal(solution)),
	 assert((problem(registrazione_al_sistema,CERT4))),
	 assert((problem(frequenza_utilizzo,nonso)))]
	explained_by [11,111]
	severity 1.
	 
	rule 112:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	 risposta(_,_,problem(registrazione_al_sistema,CERT4)), CERT4 > 0.5]
	==>
	[question_id(ID),
	 domanda(ID,rimane_su_pagina_iniziale,VAL,incert),
	 retract(certezza(C)),
	 calcola_certezza([C,CERT4],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert(risposta(ID,incert,problem(rimane_su_pagina_iniziale,VAL)))]
	 explained_by [11]
	 severity 2.

		rule 1121:
		[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
	     risposta(_,_,problem(rimane_su_pagina_iniziale,CERT4)), CERT4 < 0.5]
		==>
		[question_id(ID),
		 domanda(ID,disconnessione_login,VAL,incert),
		 assert(risposta(ID,incert,problem(disconnessione_login,VAL)))]
		 explained_by [11]
		 severity 2.

				rule 11211:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,  
                 risposta(_,_,problem(disconnessione_login,CERT4)), CERT4 > 0.5]
				==>
				[question_id(ID),
				 domanda(ID,dispositivi,VAL,incert),
				 retract(certezza(C)),
				 calcola_certezza([C,CERT4,VAL],0.8,NEWVAL),
				 assert(certezza(NEWVAL)),
				 assert(risposta(ID,incert,problem(dispositivi,VAL)))]					
				 explained_by [11]
				 severity 2.		
 
						rule 112111:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
                         problem(disconnessione_login,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(dispositivi,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(durata_nel_tempo,prolungata)),
						 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), 
						 VAL >= 15]
						==>
						[assert(incident(hw)),
						 assert(dettaglio(dispositivo)),
						 assert(soluzione(sostituzione)),
						 assert(goal(solution)),
						 assert((problem(dispositivi,CERT5))),
		                 assert((problem(durata_nel_tempo,prolungata))),
		                 assert((problem(frequenza_problemi_analoghi,VAL)))]
						 explained_by [11,11211,112111]
				         severity 2.	
		
						rule 112112:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
                         problem(disconnessione_login,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(dispositivi,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(durata_nel_tempo,prolungata)),
						 risposta(_,_,problem(frequenza_utilizzo,alta))]
						==>
						[assert(incident(err_generico)),
						 assert(dettaglio(dispositivo)),
						 assert(soluzione(collegamento)),
						 assert(goal(solution)),
						 assert((problem(dispositivi,CERT5))),
		                 assert((problem(durata_nel_tempo,prolungata))),
		                 assert((problem(frequenza_utilizzo,alta)))]
						 explained_by [11,11211,112112]
				         severity 2.
				         	     	            
						rule 112113:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
                         problem(disconnessione_login,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(dispositivi,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), 
						 VAL >= 0, VAL < 15]
						==>
						[assert(incident(sw)),
						 assert(dettaglio(client)),
						 assert(soluzione(impostazioni)),
						 assert(goal(solution)),
						 assert((problem(dispositivi,CERT5))),
		                 assert((problem(frequenza_problemi_analoghi,VAL)))]
						 explained_by [11,11211,112113]
				         severity 2.

					rule 112114:
					[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
                     problem(disconnessione_login,CERT4), CERT4 > 0.5,
					 risposta(_,_,problem(dispositivi,CERT5)), CERT5 < 0.5,
					 risposta(_,_,problem(frequenza_utilizzo,bassa))]
					==>
					[assert(incident(sw)),
					 assert(dettaglio(client)),
					 assert(soluzione(proxy)),
					 assert(goal(solution)),
					 assert((problem(dispositivi,CERT5))),
					 assert((problem(frequenza_utilizzo,bassa)))]
					 explained_by [11,11211,112114]
			         severity 2.
			    
					rule 112115:
					[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
                     problem(disconnessione_login,CERT4), CERT4 > 0.5,
                     risposta(_,_,problem(dispositivi,CERT5)), CERT5 < 0.5,
					 risposta(_,_,problem(durata_nel_tempo,intermittente))]
					==>
					[assert(incident(network)),
					 assert(dettaglio(temporaneo)),
					 assert(soluzione(refresh)),
					 assert(goal(solution)),
					 assert((problem(dispositivi,CERT5))),
					 assert((problem(durata_nel_tempo,intermittente)))]
					 explained_by [11,11211,112115]
			         severity 2.		

				rule 11212:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
                 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                 risposta(_,_,problem(disconnessione_login,CERT4)), CERT4 < 0.5]
				==>
				[question_id(ID),
				 domanda(ID,connettivita,VAL,incert),
				 assert(risposta(ID,incert,problem(connettivita,VAL)))]					
				 explained_by [11]
				 severity 2.	
		
								rule 112121:
								[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                             problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
								 risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5,
								 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
								 risposta(_,_,problem(frequenza_utilizzo,bassa)),
								 risposta(_,_,problem(durata_nel_tempo,prolungata))]
								==>
								[assert(incident(err_generico)),
			                     assert(dettaglio(client)),
							     assert(soluzione(controlla_url)),
								 assert(goal(solution)),
								 retract(certezza(C)),
					             calcola_certezza([C,CERT4],0.8,NEWVAL),
					             assert(certezza(NEWVAL)),
								 assert((problem(connettivita,CERT4))),
								 assert((problem(problemi_analoghi,CERT5))),
								 assert((problem(frequenza_utilizzo,bassa))),
							     assert((problem(durata_nel_tempo,prolungata)))]
								 explained_by [11,112121]
								 severity 2.
						 
								rule 112122:
								[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                             problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	                             risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5,
								 risposta(_,_,problem(durata_nel_tempo,intermittente)),
								 risposta(_,_,problem(frequenza_utilizzo,alta))]
								==>
								[assert(incident(sw)),
			                     assert(dettaglio(client)),
							     assert(soluzione(browser)),
								 assert(goal(solution)),
								 retract(certezza(C)),
					             calcola_certezza([C,CERT4],0.8,NEWVAL),
					             assert(certezza(NEWVAL)),
								 assert((problem(connettivita,CERT4))),
								 assert((problem(durata_nel_tempo,intermittente))),
							     assert((problem(frequenza_utilizzo,alta)))]
								 explained_by [11,112122]
								 severity 2.
								 
								 rule 112123:
								[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                             problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	                             risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5,
								 risposta(_,_,problem(durata_nel_tempo,prolungata))]
								==>
								[question_id(ID),
		                         domanda(ID,err_code,VAL,numeric),
		                         retract(certezza(C)),
					             calcola_certezza([C,CERT4],0.8,NEWVAL),
					             assert(certezza(NEWVAL)),
								 assert((problem(connettivita,CERT4))),
								 assert(risposta(ID,numeric,problem(err_code,VAL))),
								 assert((problem(durata_nel_tempo,prolungata)))]
								 explained_by [11,112123]
								 severity 2.
								 
										rule 1121231:
										[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
				                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
				                         problem(durata_nel_tempo,prolungata),
				                         problem(connettivita,CERT4), CERT4 > 0.5,
										 risposta(_,_,problem(err_code,VAL)), VAL == 401]
										==>
										[assert(incident(security)),
						                 assert(dettaglio(firewall)),
						                 assert(soluzione(permessi)),
						                 assert(goal(solution)),
						                 assert((problem(err_code,VAL)))]
							             explained_by [11,112123,1121231]
							             severity 1.	
				
										rule 1121232:
										[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
				                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
				                         problem(durata_nel_tempo,prolungata),
				                         problem(connettivita,CERT4), CERT4 > 0.5,
				                         risposta(_,_,problem(err_code,VAL)), VAL == 403]
										==>
										[assert(incident(security)),
						                 assert(dettaglio(firewall)),
						                 assert(soluzione(permessi)),
						                 assert(goal(solution)),
						                 assert((problem(err_code,VAL)))]
							             explained_by [11,112123,1121232]
							             severity 1.	
		       		

		rule 1122:
		[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
		 risposta(_,_,problem(rimane_su_pagina_iniziale,CERT4)), CERT4 > 0.5]
		==>
		[question_id(ID),
		 domanda(ID,tentativi_login,VAL,numeric),
		 retract(certezza(C)),
		 calcola_certezza([C,CERT4],0.8,NEWVAL),
		 assert(certezza(NEWVAL)),
		 assert(risposta(ID,numeric,problem(tentativi_login,VAL))),
		 assert((problem(rimane_su_pagina_iniziale,CERT4)))]
		 explained_by [11,1122]
		 severity 2.
						     
						rule 11221:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	                     problem(rimane_su_pagina_iniziale,CERT4), CERT4 > 0.5,
	                     risposta(_,_,problem(tentativi_login,VAL)),
						 VAL >=0,
						 VAL =< 1]
						==>
						[assert(incident(security)),
						 assert(dettaglio(user)),
						 assert(soluzione(reinsert)),
						 assert(goal(solution)),
						 assert((problem(tentativi_login,VAL)))]
						 explained_by [11,1122,11221]
						 severity 1.
						 
						rule 11222:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	                     problem(rimane_su_pagina_iniziale,CERT4), CERT4 > 0.5,
	                     risposta(_,_,problem(tentativi_login,VAL)),
						 VAL > 1,
						 VAL =< 3]
						==>
						[assert(incident(security)),
						 assert(dettaglio(user)),
						 assert(soluzione(dimenticata)),
						 assert(goal(solution)),
						 assert((problem(tentativi_login,VAL)))]
						 explained_by [11,1122,11222]
						 severity 2.
			
						rule 11223:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	                     problem(rimane_su_pagina_iniziale,CERT4), CERT4 > 0.5,
	                     risposta(_,_,problem(tentativi_login,VAL)),
						 VAL >= 4]
						==>
						[assert(incident(security)),
						 assert(dettaglio(user)),
						 assert(soluzione(bloccata)),
						 assert(goal(solution)),
						 assert((problem(tentativi_login,VAL)))]
						 explained_by [11,1122,11223]
						 severity 2.									 

						rule 11224:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
	                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5, 
	                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
	                     problem(rimane_su_pagina_iniziale,CERT4), CERT4 > 0.5,
	                     risposta(_,_,problem(tentativi_login,VAL)),
						 risposta(_,_,problem(frequenza_utilizzo,bassa)),
						 VAL >= 4]
						==>
						[assert(incident(security)),
						 assert(dettaglio(user)),
						 assert(soluzione(scaduta)),
						 assert(goal(solution)),
						 assert((problem(tentativi_login,VAL))),
						 assert((problem(frequenza_utilizzo,bassa)))]
						 explained_by [11,1122,11224]
						 severity 2.	


		
		
rule 12:
[risposta(_,_,problem(mancata_autenticazione,CERT1)), CERT1 > 0.5,
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5,
 risposta(_,_,problem(durata_nel_tempo,intermittente))]
   ==>
	[question_id(ID),
	 domanda(ID,connettivita,VAL,incert),
	 retract(certezza(C)),
	 calcola_certezza([C,CERT1,CERT2,CERT3],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert(risposta(ID,incert,problem(connettivita,VAL))),
     assert((problem(mancata_autenticazione,CERT1))),
     assert((problem(piu_applicazioni_coinvolte,CERT2))),
     assert((problem(piu_utenti_coinvolti,CERT3))),
     assert((problem(durata_nel_tempo,intermittente)))]					
explained_by [12]
severity 2.

		rule 121:
		[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
         problem(durata_nel_tempo,intermittente),
		 risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5]
		==>
		[question_id(ID),
		 domanda(ID,err_code,VAL,numeric),
		 retract(certezza(C)),
	     calcola_certezza([C,CERT4],0.8,NEWVAL),
	     assert(certezza(NEWVAL)),
		 assert(risposta(ID,numeric,problem(err_code,VAL))),
		 assert((problem(connettivita,CERT4)))]					
		explained_by [12,121]
		severity 2.

					rule 1211:
					[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                     problem(durata_nel_tempo,intermittente),
                     problem(connettivita,CERT4), CERT4 > 0.5,
                     risposta(_,_,problem(err_code,VAL)), 
					 VAL == 404,
                     risposta(_,_,problem(frequenza_problemi_analoghi,FREQ)), 
                     FREQ >= 5]
					==>
					[assert(incident(sw)),
	                 assert(dettaglio(db)),
	                 assert(soluzione(stabilita)),
	                 assert(goal(solution)),
                     assert((problem(err_code,VAL))),
                     assert((problem(frequenza_problemi_analoghi,FREQ)))]
		             explained_by [12,121,1211]
		             severity 3.
    
		       		rule 1212:
					[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                     problem(durata_nel_tempo,intermittente),
                     problem(connettivita,CERT4), CERT4 > 0.5,
                     risposta(_,_,problem(err_code,VAL)), 
					 VAL == 404,
                     risposta(_,_,problem(problemi_analoghi,CERT2)), 
                     CERT2 < 0.5]
					==>
					[assert(incident(sw)),
	                 assert(dettaglio(db)),
	                 assert(soluzione(refresh)),
	                 assert(goal(solution)),
                     assert((problem(err_code,VAL))),
                     assert((problem(problemi_analoghi,CERT2)))]
		             explained_by [12,121,1212]
		             severity 2.     
		       
					rule 1213:
					[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                     problem(durata_nel_tempo,intermittente),
                     problem(connettivita,CERT4), CERT4 > 0.5,
                     risposta(_,_,problem(err_code,VAL)), 
					 VAL == 500,
				     risposta(_,_,problem(problemi_analoghi,CERT2)), 
				     CERT2 < 0.5]
					==>
					[assert(incident(sw)),
					 assert(dettaglio(serv)),
					 assert(soluzione(refresh)),
					 assert(goal(solution)),
                     assert((problem(err_code,VAL))),
                     assert((problem(problemi_analoghi,CERT2)))]
					 explained_by [12,121,1213]
					 severity 2.			

					rule 1214:
					[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                     problem(durata_nel_tempo,intermittente),
                     problem(connettivita,CERT4), CERT4 > 0.5,
                     risposta(_,_,problem(err_code,VAL)), 
					 VAL == 500,
				     risposta(_,_,problem(frequenza_problemi_analoghi,FREQ)), 
				     FREQ >= 5]
					 ==>
					[assert(incident(sw)),
					 assert(dettaglio(serv)),
					 assert(soluzione(stabilita)),
					 assert(goal(solution)),
                     assert((problem(err_code,VAL))),
                     assert((problem(frequenza_problemi_analoghi,FREQ)))]
					explained_by [12,121,1214]
					severity 3.		
				
       rule 122:
		[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
         problem(durata_nel_tempo,intermittente),
         risposta(_,_,problem(connettivita,CERT4)), CERT4 < 0.5]
		    ==>
			[question_id(ID),
			 domanda(ID,lentezza_server,VAL,incert),
			 retract(certezza(C)),
			 calcola_certezza([C,VAL],0.8,NEWVAL),
			 assert(certezza(NEWVAL)),
			 assert(risposta(ID,incert,problem(lentezza_server,VAL)))]
		explained_by [12]
		severity 3.
           
				rule 1222:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                 problem(durata_nel_tempo,intermittente),
				 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
				 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 0, VAL < 5]
				==>
				[assert(incident(hw)),
				 assert(dettaglio(serv)),
				 assert(soluzione(upgrade)),
				 assert(goal(solution)),
				 assert((problem(lentezza_server,CERT4))),
				 assert((problem(frequenza_problemi_analoghi,VAL)))]					
				 explained_by [12,1222]
				 severity 3.
    
				rule 1223:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                 problem(durata_nel_tempo,intermittente),
                 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
				 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5]
				==>
				[assert(incident(hw)),
				 assert(dettaglio(cpuram)),
				 assert(soluzione(sostituzione)),
				 assert(goal(solution)),
				 assert((problem(lentezza_server,CERT4))),
				 assert((problem(frequenza_problemi_analoghi,VAL)))]					
				 explained_by [12,1223]
				 severity 3.     
   
    
    
          
rule 13:
[risposta(_,_,problem(mancata_autenticazione,CERT1)), CERT1 > 0.5,
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5,
 risposta(_,_,problem(durata_nel_tempo,prolungata))]
    ==>
	[question_id(ID),
	 domanda(ID,lentezza_server,VAL,incert),
	 retract(certezza(C)),
	 calcola_certezza([C,CERT1,CERT2,CERT3],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert(risposta(ID,incert,problem(lentezza_server,VAL))),
     assert((problem(mancata_autenticazione,CERT1))),
     assert((problem(piu_applicazioni_coinvolte,CERT2))),
     assert((problem(piu_utenti_coinvolti,CERT3))),
     assert((problem(durata_nel_tempo,prolungata)))]					
explained_by [13]
severity 3.

	rule 131:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
     problem(durata_nel_tempo,prolungata),
	 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 < 0.5]
	==>
	[question_id(ID),
	 domanda(ID,connettivita,VAL,incert),
	 assert(risposta(ID,incert,problem(connettivita,VAL)))]				
	 explained_by [13]
	 severity 3.
	 
    		rule 1311:
			[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
             problem(durata_nel_tempo,prolungata),
			 risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5]
			==>
			[question_id(ID),
			 domanda(ID,err_code,VAL,numeric),
			 retract(certezza(C)),
             calcola_certezza([C,CERT4],0.8,NEWVAL),
             assert(certezza(NEWVAL)),
			 assert(risposta(ID,numeric,problem(err_code,VAL))),
			 assert((problem(connettivita,CERT4)))]					
			 explained_by [13,1311]
			 severity 3.
		       
				      rule 13111:
					   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                        problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                        problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                        problem(connettivita,CERT4), CERT4 > 0.5,
                        problem(durata_nel_tempo,prolungata),
					    risposta(_,_,problem(err_code,VAL)), 
					    VAL == 404,
	                    risposta(_,_,problem(minuti,D)), 
	                    D >= 10]
					   ==>
					   [assert(incident(crash)),
			            assert(dettaglio(down)),
			            assert(soluzione(db)),
			            assert(goal(solution)),
	                    assert((problem(err_code,VAL))),
	                    assert((problem(minuti,D)))]
				        explained_by [13,1311,13111]
				        severity 3.

					  rule 13112:
					   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                        problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                        problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                        problem(connettivita,CERT4), CERT4 > 0.5,
                        problem(durata_nel_tempo,prolungata),
					    risposta(_,_,problem(err_code,VAL)), VAL == 500,
				        risposta(_,_,problem(minuti,D)), D >= 10]
					   ==>
					   [assert(incident(crash)),
						assert(dettaglio(down)),
						assert(soluzione(serv)),
						assert(goal(solution)),
                        assert((problem(err_code,VAL))),
                        assert((problem(minuti,D)))]
						explained_by [13,1311,13112]
						severity 3.		

					  rule 13113:
					   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                        problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                        problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                        problem(durata_nel_tempo,prolungata),
                        problem(connettivita,CERT4), CERT4 > 0.5,
                        risposta(_,_,problem(err_code,VAL)), VAL == 502,
				        risposta(_,_,problem(minuti,D)), D >= 10]
					   ==>
					   [assert(incident(crash)),
						assert(dettaglio(down)),
						assert(soluzione(serv)),
						assert(goal(solution)),
                        assert((problem(err_code,VAL))),
                        assert((problem(minuti,D)))]
						explained_by [13,1311,13113]
						severity 3.		

						rule 13114:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                         problem(durata_nel_tempo,prolungata),
                         problem(connettivita,CERT4), CERT4 > 0.5,
                         risposta(_,_,problem(err_code,VAL)), VAL == 503]
						==>
						[assert(incident(sw)),
		                 assert(dettaglio(serv)),
		                 assert(soluzione(manutenzione)),
		                 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
			             explained_by [11,1311,13114]
			             severity 1.
			             
			            rule 13115:
					   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                        problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                        problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                        problem(connettivita,CERT4), CERT4 > 0.5,
                        problem(durata_nel_tempo,prolungata),
					    risposta(_,_,problem(err_code,VAL)), 
					    VAL == 404,
	                    risposta(_,_,problem(minuti,D)), 
	                    D < 10]
					   ==>
					   [assert(incident(sovraccarico)),
			            assert(dettaglio(serv)),
			            assert(soluzione(db)),
			            assert(goal(solution)),
	                    assert((problem(err_code,VAL))),
	                    assert((problem(minuti,D)))]
				        explained_by [13,1311,13115]
				        severity 3.
				        
				       rule 13116:
					   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                        problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                        problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                        problem(connettivita,CERT4), CERT4 > 0.5,
                        problem(durata_nel_tempo,prolungata),
					    risposta(_,_,problem(err_code,VAL)), 
					    VAL == 500,
	                    risposta(_,_,problem(minuti,D)), 
	                    D < 10]
					   ==>
					   [assert(incident(sovraccarico)),
			            assert(dettaglio(serv)),
			            assert(soluzione(elaborazione)),
			            assert(goal(solution)),
	                    assert((problem(err_code,VAL))),
	                    assert((problem(minuti,D)))]
				        explained_by [13,1311,13116]
				        severity 3.
				       
				       	rule 13117:
					   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                        problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                        problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                        problem(connettivita,CERT4), CERT4 > 0.5,
                        problem(durata_nel_tempo,prolungata),
					    risposta(_,_,problem(err_code,VAL)), 
					    VAL == 502,
	                    risposta(_,_,problem(minuti,D)), 
	                    D < 10]
					   ==>
					   [assert(incident(sovraccarico)),
			            assert(dettaglio(network)),
			            assert(soluzione(traffico)),
			            assert(goal(solution)),
	                    assert((problem(err_code,VAL))),
	                    assert((problem(minuti,D)))]
				        explained_by [13,1311,13117]
				        severity 3.
			             
			             

			rule 134:
			[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
             risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
			 risposta(_,_,problem(minuti,D)), D > 0, D < 5]
			==>
			[assert(incident(sovraccarico)),
			 assert(dettaglio(serv)),
			 assert(soluzione(processi)),
			 assert(goal(solution)),
			 retract(certezza(C)),
	         calcola_certezza([C,CERT4],0.8,NEWVAL),
	         assert(certezza(NEWVAL)),
			 assert((problem(lentezza_server,CERT4))),
			 assert((problem(minuti,D)))]					
			 explained_by [13,134]
			 severity 3.
		
			rule 135:
			[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
             risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
			 risposta(_,_,problem(minuti,VAL)), VAL >= 5]
			==>
			[question_id(ID),
		     domanda(ID,blocco,VAL,incert),
		     retract(certezza(C)),
		     calcola_certezza([C,CERT4,VAL],NEWVAL),
		     assert(certezza(NEWVAL)),
		     assert(risposta(ID,incert,problem(blocco,VAL))),
			 assert((problem(lentezza_server,CERT4))),
			 assert((problem(minuti,VAL)))]					
			 explained_by [13,135]
			 severity 3.
		
			    		rule 1351:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                         problem(lentezza_server,CERT4), CERT4 > 0.5,
                         risposta(_,_,problem(blocco,CERT5)), CERT5 > 0.5]
						==>
						[assert(incident(crash)),
		                 assert(dettaglio(hang)),
		                 assert(soluzione(serv)),
		                 assert(goal(solution)),
						 assert((problem(blocco,CERT5)))]					
						 explained_by [13,135,1351]
						 severity 3.
 
 
                           
rule 14:
[risposta(_,_,problem(mancata_autenticazione,CERT1)), CERT1 > 0.5,
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
	==>
	[question_id(ID),
	 domanda(ID,non_affidabile,VAL,incert),
	 retract(certezza(C)),
	 calcola_certezza([C,CERT1,CERT2,CERT3],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert(risposta(ID,incert,problem(non_affidabile,VAL))),
     assert((problem(mancata_autenticazione,CERT1))),
     assert((problem(piu_applicazioni_coinvolte,CERT2))),
     assert((problem(piu_utenti_coinvolti,CERT3)))]					
explained_by [14]
severity 2.		

			rule 141:
			[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
			 risposta(_,_,problem(non_affidabile,CERT4)), CERT4 > 0.5]
			==>
		    [assert(incident(security)),
		     assert(dettaglio(firewall)),
		     assert(soluzione(antivirus)),
		     assert(goal(solution)),
		     retract(certezza(C)),
	         calcola_certezza([C,CERT4],0.8,NEWVAL),
	         assert(certezza(NEWVAL)),
		     assert((problem(non_affidabile,CERT4)))]
			 explained_by [14,141]
		     severity 2.

	rule 142:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
     risposta(_,_,problem(non_affidabile,CERT4)), CERT4 < 0.5]
	==>
	[question_id(ID),
     domanda(ID,malware,VAL,incert),
     assert(risposta(ID,incert,problem(malware,VAL)))]
	 explained_by [14]
     severity 2.
							
				rule 1421:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(malware,CERT4)), CERT4 > 0.5,
				 risposta(_,_,problem(durata_nel_tempo,prolungata))]
				==>
				[question_id(ID),
		         domanda(ID,browser,VAL,incert),
		         retract(certezza(C)),
		         calcola_certezza([C,CERT4],NEWVAL),
		         assert(certezza(NEWVAL)),
		         assert(risposta(ID,incert,problem(browser,VAL))),
		         assert((problem(malware,CERT4))),
		         assert((problem(durata_nel_tempo,prolungata)))]
			    explained_by [14,1421]
		        severity 2.			
		        	
						rule 14211:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				         problem(malware,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(frequenza_utilizzo,alta))]
						==>
						[assert(incident(sw)),
		                 assert(dettaglio(client)),
		                 assert(soluzione(malware)),
		                 assert(goal(solution)),
		                 retract(certezza(C)),
		                 calcola_certezza([C,CERT5],NEWVAL),
		                 assert(certezza(NEWVAL)),
		                 assert((problem(browser,CERT5))),
	                     assert((problem(frequenza_utilizzo,alta)))]
		               	explained_by [14,1421,14211]
		                severity 2.	
									
						rule 14212:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				         problem(malware,CERT4), CERT4 > 0.5,
						 risposta(_,problem(browser,CERT5)), CERT5 < 0.5]
						==>
						[assert(incident(security)),
		                 assert(dettaglio(malware)),
		                 assert(soluzione(estensioni)),
		                 assert(goal(solution)),
		                 assert((problem(browser,CERT5)))] 	
		               	explained_by [14,1421,14212]
		                severity 2.	

		rule 1422:
		[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
		 risposta(_,_,problem(malware,CERT4)), CERT4 < 0.5]
		    ==>
			[question_id(ID),
			 domanda(ID,lentezza_pc,VAL,incert),
			 assert(risposta(ID,incert,problem(lentezza_pc,VAL)))]				
		explained_by [14]
		severity 2.

				rule 14221:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(lentezza_pc,CERT4)), CERT4 > 0.5]
				==>
				[question_id(ID),
				 domanda(ID,browser,VAL,incert),
				 retract(certezza(C)),
			     calcola_certezza([C,CERT4],NEWVAL),
			     assert(certezza(NEWVAL)),
				 assert(risposta(ID,incert,problem(browser,VAL))),
				 assert((problem(lentezza_pc,CERT4)))]					
				 explained_by [14,14221]
				 severity 2.

			    		rule 142211:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 < 0.5,
						 risposta(_,_,problem(durata_nel_tempo,prolungata))]
						==>
						[assert(incident(sovraccarico)),
			             assert(dettaglio(client)),
			             assert(soluzione(schede)),
			             assert(goal(solution)),
						 assert((problem(browser,CERT5))),
		                 assert((problem(durata_nel_tempo,prolungata)))]					
						 explained_by [14,14221,142211]
						 severity 2.

						rule 142212:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
                         risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(problemi_analoghi,CERT6)), CERT6 > 0.5]
						==>
						[assert(incident(hw)),
						 assert(dettaglio(client)),
						 assert(soluzione(upgrade)),
						 assert(goal(solution)),
						 retract(certezza(C)),
			             calcola_certezza([C,CERT5,CERT6],NEWVAL),
			             assert(certezza(NEWVAL)),
						 assert((problem(browser,CERT5))),
						 assert((problem(problemi_analoghi,CERT6)))]					
						 explained_by [14,14221,142212]
						 severity 3.
		   
			    		rule 142213:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(minuti,VAL)), VAL > 0, VAL < 10]
						==>
						[assert(incident(sovraccarico)),
			             assert(dettaglio(client)),
			             assert(soluzione(applicazioni)),
			             assert(goal(solution)),
			             retract(certezza(C)),
			             calcola_certezza([C,CERT5],NEWVAL),
			             assert(certezza(NEWVAL)),
						 assert((problem(browser,CERT5))),
						 assert((problem(minuti,VAL)))]
						 explained_by [14,14221,142213]
						 severity 2.

			    		rule 142214:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
					     risposta(_,_,problem(minuti,VAL)), VAL >= 10]
							==>
							[question_id(ID),
					         domanda(ID,blocco,VALR,incert),
					         retract(certezza(C)),
					         calcola_certezza([C,CERT5],0.8,NEWVAL),
					         assert(certezza(NEWVAL)),
					         assert(risposta(ID,incert,problem(blocco,VALR))),
							 assert((problem(browser,CERT5))),
							 assert((problem(minuti,VAL)))]					
							 explained_by [14,14221]
							 severity 3.
					
						    		rule 1422141:
									[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
			                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
			                         risposta(_,_,problem(blocco,CERT5)), CERT5 > 0.5]
									==>
									[assert(incident(crash)),
					                 assert(dettaglio(hang)),
					                 assert(soluzione(client)),
					                 assert(goal(solution)),
									 assert((problem(blocco,CERT5)))]					
									 explained_by [14,14221,142214,1422141]
									 severity 3.

			    		rule 142215:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(durata_nel_tempo,intermittente))]
						==>
						[assert(incident(sovraccarico)),
			             assert(dettaglio(client)),
			             assert(soluzione(utilizzo)),
			             assert(goal(solution)),
			             retract(certezza(C)),
					     calcola_certezza([C,CERT5],0.8,NEWVAL),
					     assert(certezza(NEWVAL)),
						 assert((problem(browser,CERT5))),
		                 assert((problem(durata_nel_tempo,intermittente)))]					
						 explained_by [14,14221,142214]
						 severity 2.

				rule 14222:
				[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(lentezza_pc,CERT4)), CERT4 < 0.5]
				==>
				[question_id(ID),
				 domanda(ID,connettivita,VAL,incert),
				 assert(risposta(ID,incert,problem(connettivita,VAL)))]			
				 explained_by [14]
				 severity 3.
	 
			    		rule 142221:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5, 
						 risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5,
						 risposta(_,_,problem(minuti,D)), D >= 10]
						==>
						[assert(incident(err_generico)),
			             assert(dettaglio(client)),
			             assert(soluzione(connessione)),
			             assert(goal(solution)),
			             retract(certezza(C)),
				         calcola_certezza([C,CERT4],0.8,NEWVAL),
				         assert(certezza(NEWVAL)),
						 assert((problem(connettivita,CERT4))),
						 assert((problem(minuti,D)))]					
						 explained_by [14,142221]
						 severity 1.
				   
			    		rule 142222:
						[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(connettivita,CERT4)), CERT4 > 0.5,
						 risposta(_,_,problem(durata_nel_tempo,intermittente))]
						==>
						[assert(incident(sw)),
			             assert(dettaglio(client)),
			             assert(soluzione(connessione)),
			             assert(goal(solution)),
			             retract(certezza(C)),
				         calcola_certezza([C,CERT4],0.8,NEWVAL),
				         assert(certezza(NEWVAL)),
						 assert((problem(connettivita,CERT4))),
						 assert((problem(durata_nel_tempo,intermittente)))]					
						 explained_by [14,142222]
						 severity 3.
				
				
				
rule 16:
[risposta(_,_,problem(mancata_autenticazione,CERT1)), CERT1 > 0.5,
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5]
	==>
     [retract(certezza(C)),
     question_id(ID),
	 domanda(ID,lentezza_rete,VAL,incert),
	 calcola_certezza([C,CERT1,CERT2,CERT3],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert(risposta(ID,incert,problem(lentezza_rete,VAL))),
     assert((problem(mancata_autenticazione,CERT1))),
     assert((problem(piu_applicazioni_coinvolte,CERT2))),
     assert((problem(piu_utenti_coinvolti,CERT3)))]					
explained_by [16]
severity 3.	

	rule 161:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
	 risposta(_,_,problem(lentezza_rete,CERT4)), CERT4 < 0.5]
	==>
	[question_id(ID),
	 domanda(ID,connettivita,VAL,incert),
	 assert(risposta(ID,incert,problem(connettivita,VAL)))]				
	 explained_by [16]
	 severity 3.

		      rule 1611:
			   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			    risposta(_,problem(connettivita,CERT4)), CERT4 > 0.5,
                risposta(_,problem(minuti,D)), D >= 10]
			   ==>
			   [assert(incident(crash)),
	            assert(dettaglio(down)),
	            assert(soluzione(network)),
	            assert(goal(solution)),
	            retract(certezza(C)),
                calcola_certezza([C,CERT4],0.8,NEWVAL),
                assert(certezza(NEWVAL)),
                assert((problem(connettivita,CERT4))),
                assert((problem(minuti,D)))]
		        explained_by [16,1611]
		        severity 3.
		       
		       rule 1612:
			   [problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
                problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			    risposta(_,problem(connettivita,CERT4)), CERT4 > 0.5,
                risposta(_,problem(minuti,D)), D > 0, D < 10]
			   ==>
			   [assert(incident(sw)),
	            assert(dettaglio(network)),
	            assert(soluzione(instradamento)),
	            assert(goal(solution)),
	            retract(certezza(C)),
                calcola_certezza([C,CERT4],0.8,NEWVAL),
                assert(certezza(NEWVAL)),
                assert((problem(connettivita,CERT4))),
                assert((problem(minuti,D)))]
		        explained_by [16,1612]
		        severity 3.
		       

	rule 162:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
     risposta(_,problem(lentezza_rete,CERT4)), CERT4 > 0.5,
	 risposta(_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5,
	 risposta(_,problem(durata_nel_tempo,intermittente))]
	==>
	[assert(incident(hw)),
	 assert(dettaglio(network)),
	 assert(soluzione(apparati)),
	 assert(goal(solution)),
	 retract(certezza(C)),
     calcola_certezza([C,CERT4],0.8,NEWVAL),
     assert(certezza(NEWVAL)),
	 assert((problem(lentezza_rete,CERT4))),
	 assert((problem(frequenza_problemi_analoghi,VAL))),
	 assert((problem(durata_nel_tempo,intermittente)))]					
	 explained_by [16,162]
	 severity 3.
	
	rule 163:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
     risposta(_,problem(lentezza_rete,CERT4)), CERT4 > 0.5,
	 risposta(_,problem(durata_nel_tempo,prolungata))]
	==>
	[assert(incident(sovraccarico)),
	 assert(dettaglio(network)),
	 assert(soluzione(traffico)),
	 assert(goal(solution)),
	 retract(certezza(C)),
     calcola_certezza([C,CERT4],0.8,NEWVAL),
     assert(certezza(NEWVAL)),
	 assert((problem(lentezza_rete,CERT4))),
	 assert((problem(durata_nel_tempo,prolungata)))]					
	 explained_by [16,163]
	 severity 3.

	rule 164:
	[problem(mancata_autenticazione,CERT1), CERT1 > 0.5,
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
     risposta(_,problem(lentezza_rete,CERT4)), CERT4 > 0.5,
     risposta(_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
	 risposta(_,problem(durata_nel_tempo,intermittente))]
	==>
	[assert(incident(network)),
	 assert(dettaglio(temporaneo)),
	 assert(soluzione(refresh)),
	 assert(goal(solution)),
	 retract(certezza(C)),
     calcola_certezza([C,CERT4],0.8,NEWVAL),
     assert(certezza(NEWVAL)),
	 assert((problem(lentezza_rete,CERT4))),
	 assert((problem(problemi_analoghi,CERT5))),
	 assert((problem(durata_nel_tempo,intermittente)))]					
	 explained_by [16,164]
	 severity 3.	
	
	

rule 20:
[risposta(_,_,problem(mancata_autenticazione,CERT1)), CERT1 < 0.5]
==>
[question_id(ID),
 domanda(ID,finalita,VAL,finalita),
 retract(certezza(C)),
 cert_risp_neg(CERT1,CERTNEW),
 calcola_certezza([C,CERTNEW],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(funzionalita,CERT1))),
 assert(risposta(ID,finalita,problem(finalita,VAL)))]
 explained_by [20]
 severity 2.


rule 21:
[problem(funzionalita,_),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
==>
[question_id(ID),
 domanda(ID,malware,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(malware,VAL)))]
 explained_by [21]
 severity 2.
		
       rule 211:
		[problem(funzionalita,_),
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
		 risposta(_,_,problem(malware,CERT4)), CERT4 > 0.5]
		==>
		[question_id(ID),
         domanda(ID,browser,VAL,incert),
         retract(certezza(C)),
         calcola_certezza([C,CERT4],0.8,NEWVAL),
         assert(certezza(NEWVAL)),
         assert(risposta(ID,incert,problem(browser,VAL))),
         assert((problem(malware,CERT4)))]
	    explained_by [21,211]
        severity 2.			
	        	
					rule 2111:
					[problem(funzionalita,_),
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
			         problem(malware,CERT4), CERT4 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,prolungata)),
					 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
					 risposta(_,_,problem(frequenza_utilizzo,alta))]
					==>
					[assert(incident(security)),
	                 assert(dettaglio(malware)),
	                 assert(soluzione(scansione)),
	                 assert(goal(solution)),
	                 retract(certezza(C)),
                     calcola_certezza([C,CERT5],0.8,NEWVAL),
                     assert(certezza(NEWVAL)),
	                 assert((problem(durata_nel_tempo,prolungata))),
	                 assert((problem(browser,CERT5))),
                     assert((problem(frequenza_utilizzo,alta)))]
	               	explained_by [21,211,2111]
	                severity 2.	
								
					rule 2112:
					[problem(funzionalita,_),
                     problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                     problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
			         problem(malware,CERT4), CERT4 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,prolungata)),
					 risposta(_,_,problem(browser,CERT5)), CERT5 < 0.5]
					==>
					[assert(incident(security)),
	                 assert(dettaglio(malware)),
	                 assert(soluzione(estensioni)),
	                 assert(goal(solution)),
	                 assert((problem(durata_nel_tempo,prolungata))),
	                 assert((problem(browser,CERT5)))] 	
	               	explained_by [21,211,2112]
	                severity 2.	
		
		rule 212:
		[problem(funzionalita,_),
		 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
		 risposta(_,_,problem(malware,CERT4)), CERT4 < 0.5]
		==>
		[question_id(ID),
		 domanda(ID,lentezza_pc,VAL,incert),
		 assert(risposta(ID,incert,problem(lentezza_pc,VAL)))]				
		explained_by [21]
		severity 2.

				rule 2121:
				[problem(funzionalita,_),
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(lentezza_pc,CERT4)), CERT4 > 0.5]
				==>
				[question_id(ID),
				 domanda(ID,browser,VAL,incert),
				 retract(certezza(C)),
			     calcola_certezza([C,CERT4],0.8,NEWVAL),
			     assert(certezza(NEWVAL)),
				 assert(risposta(ID,incert,problem(browser,VAL))),
				 assert((problem(lentezza_pc,CERT4)))]					
				 explained_by [21,2121]
				 severity 2.

			    		rule 21211:
						[problem(funzionalita,_),
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 < 0.5]
						==>
						[assert(incident(sovraccarico)),
			             assert(dettaglio(client)),
			             assert(soluzione(schede)),
			             assert(goal(solution)),
						 assert((problem(browser,CERT5)))]					
						 explained_by [21,2121,21211]
						 severity 2.

						rule 21212:
						[problem(funzionalita,_),
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
                         risposta(_,_,problem(durata_nel_tempo,prolungata)),
                         risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5]
						==>
						[assert(incident(hw)),
						 assert(dettaglio(client)),
						 assert(soluzione(upgrade)),
						 assert(goal(solution)),
						 retract(certezza(C)),
			             calcola_certezza([C,CERT5],0.8,NEWVAL),
			             assert(certezza(NEWVAL)),
						 assert((problem(browser,CERT5))),
						 assert((problem(frequenza_problemi_analoghi,VAL)))]					
						 explained_by [21,2121,21212]
						 severity 3.
		   
			    		rule 21213:
						[problem(funzionalita,_),
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(minuti,VAL)), VAL >= 0, VAL < 10]
						==>
						[assert(incident(sovraccarico)),
			             assert(dettaglio(client)),
			             assert(soluzione(applicazioni)),
			             assert(goal(solution)),
			             retract(certezza(C)),
			             calcola_certezza([C,CERT5],0.8,NEWVAL),
			             assert(certezza(NEWVAL)),
						 assert((problem(browser,CERT5))),
						 assert((problem(minuti,VAL)))]			
						 explained_by [21,2121,21213]
						 severity 2.
		
					    rule 21214:
						[problem(funzionalita,_),
                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
                         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
                         problem(lentezza_pc,CERT4), CERT4 > 0.5,
						 risposta(_,_,problem(browser,CERT5)), CERT5 > 0.5,
						 risposta(_,_,problem(minuti,VAL)), VAL >= 10]
						==>
						[assert(incident(crash)),
			             assert(dettaglio(hang)),
			             assert(soluzione(client)),
			             assert(goal(solution)),
			             retract(certezza(C)),
			             calcola_certezza([C,CERT5],0.8,NEWVAL),
			             assert(certezza(NEWVAL)),
						 assert((problem(browser,CERT5))),
						 assert((problem(minuti,VAL)))]			
						 explained_by [21,2121,21214]
						 severity 2.
		
 rule 213:
[problem(funzionalita,_),
 risposta(_,_,problem(finalita,report)),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
    ==>
[question_id(ID),
 domanda(ID,doc,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(finalita,report))),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(doc,VAL)))]
 explained_by [213]
 severity 2.

		     			rule 2131:
						[problem(funzionalita,_),
						 problem(finalita,report),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(doc,CERT4)), CERT4 < 0.5]
						    ==>
						[assert(incident(sw)),
				         assert(dettaglio(client)),
				         assert(soluzione(applicazione)),
				         assert(goal(solution)),
				         assert((problem(doc,CERT4)))]
						 explained_by [213,2131]
						 severity 3.	
						
						rule 2132:
						[problem(funzionalita,_),
						 problem(finalita,report),
						 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
						 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(doc,CERT4)), CERT4 > 0.5]
						==>
						[question_id(ID),
						 domanda(ID,transfer,VAL,incert),
						 retract(certezza(C)),
						 calcola_certezza([C,CERT4],0.8,NEWVAL),
						 assert(certezza(NEWVAL)),
						 assert(risposta(ID,incert,problem(transfer,VAL)))]
						 explained_by [213]
						 severity 2.
						          
								 rule 21321:
								[problem(funzionalita,_),
								 problem(finalita,report),
								 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
								 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
								 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5,
								 risposta(_,_,problem(transfer,CERT4)), CERT4 > 0.5]
								==>
						        [assert(incident(sw)),
				                 assert(dettaglio(client)),
				                 assert(soluzione(trasferimento)),
				                 assert(goal(solution)),
				                 retract(certezza(C)),
						         calcola_certezza([C,CERT4],0.8,NEWVAL),
						         assert(certezza(NEWVAL)),
				                 assert((problem(frequenza_problemi_analoghi,VAL))),
				                 assert((problem(transfer,CERT4)))]
								 explained_by [213,21321]
								 severity 2.
						
								rule 21322:
								[problem(funzionalita,_),
								 problem(finalita,report),
								 problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
								 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
								 risposta(_,_,problem(transfer,CERT4)), CERT4 < 0.5]
								==>
								[question_id(ID),
								 domanda(ID,lentezza_pc,VAL,incert),
								 assert(risposta(ID,incert,problem(lentezza_pc,VAL)))]
								 explained_by [213]
								 severity 2.
								
	
rule 214:
[problem(funzionalita,_),
 risposta(_,_,problem(finalita,altro)),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
    ==>
[question_id(ID),
 domanda(ID,stampa,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(finalita,report))),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(stampa,VAL)))]
 explained_by [214]
 severity 2.
     
       		          rule 2141:
						[problem(funzionalita,_),
						 problem(finalita,altro),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(stampa,CERT4)), CERT4 > 0.5]
						    ==>
						[assert(incident(err_generico)),
				         assert(dettaglio(dispositivo)),
				         assert(soluzione(stampa)),
				         assert(goal(solution)),
				         retract(certezza(C)),
                         calcola_certezza([C,CERT4],0.8,NEWVAL),
                         assert(certezza(NEWVAL)),
				         assert((problem(stampa,CERT4)))]
						 explained_by [214,2141]
						 severity 1.	

		                rule 2142:
						[problem(funzionalita,_),
						 problem(finalita,altro),
				         risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
				         risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5,
						 risposta(_,_,problem(stampa,CERT4)), CERT4 < 0.5]
						    ==>
						[question_id(ID),
						 domanda(ID,orario,VAL,incert),
						 assert(risposta(ID,incert,problem(orario,VAL)))]
						 explained_by [214]
						 severity 2.
	
			                    rule 21421:
								[problem(funzionalita,_),
								 problem(finalita,altro),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
								 risposta(_,_,problem(orario,CERT4)), CERT4 > 0.5]
								    ==>
								[assert(incident(sw)),
						         assert(dettaglio(client)),
						         assert(soluzione(ntp)),
						         assert(goal(solution)),
						         retract(certezza(C)),
                                 calcola_certezza([C,CERT4],0.8,NEWVAL),
                                 assert(certezza(NEWVAL)),
						         assert((problem(orario,CERT4)))]
								 explained_by [214,21421]
								 severity 2.
							 
				                 rule 21422:
								[problem(funzionalita,_),
								 problem(finalita,altro),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
								 risposta(_,_,problem(orario,CERT4)), CERT4 < 0.5]
								    ==>
								[question_id(ID),
								 domanda(ID,lentezza_pc,VAL,incert),
								 assert(risposta(ID,incert,problem(lentezza_pc,VAL)))]
								 explained_by [214]
								 severity 3.
	
	
	    rule 215:
		[problem(funzionalita,_),
		 risposta(_,_,problem(finalita,mail)),
		 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
		 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
		    ==>
		[question_id(ID),
		 domanda(ID,allegati,VAL,incert),
		 retract(certezza(C)),
		 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
		 assert(certezza(NEWVAL)),
		 assert((problem(finalita,mail))),
		 assert((problem(piu_applicazioni_coinvolte,CERT2))),
		 assert((problem(piu_utenti_coinvolti,CERT3))),
		 assert(risposta(ID,incert,problem(allegati,VAL)))]
		 explained_by [215]
		 severity 2.
     
       		          rule 2151:
						[problem(funzionalita,_),
						 problem(finalita,mail),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(allegati,CERT4)), CERT4 > 0.5]
						    ==>
						[assert(incident(err_generico)),
				         assert(dettaglio(client)),
				         assert(soluzione(mail)),
				         assert(goal(solution)),
				         retract(certezza(C)),
                         calcola_certezza([C,CERT4],0.8,NEWVAL),
                         assert(certezza(NEWVAL)),
				         assert((problem(allegati,CERT4)))]
						 explained_by [215,2151]
						 severity 1.	

		                rule 2152:
						[problem(funzionalita,_),
						 problem(finalita,mail),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(allegati,CERT4)), CERT4 < 0.5]
						    ==>
						[question_id(ID),
						 domanda(ID,lentezza_pc,VAL,incert),
						 assert(risposta(ID,incert,problem(lentezza_pc,VAL)))]
						 explained_by [215]
						 severity 3.
	
	
		
rule 22:
[problem(funzionalita,_),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5]
==>
[question_id(ID),
 domanda(ID,funzioni,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(funzioni,VAL)))]
 explained_by [22]
 severity 2.
 
		      rule 220:
			  [problem(funzionalita,_),
	           problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
	           problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			   risposta(_,_,problem(funzioni,CERT4)), CERT4 > 0.5]
			  ==>
				[question_id(ID),
				 domanda(ID,disconnessione_utilizzo,VAL,incert),
				 retract(certezza(C)),
				 calcola_certezza([C,CERT4],0.8,NEWVAL),
				 assert(certezza(NEWVAL)),
				 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
				 explained_by [22]
				 severity 2.

		            rule 221:
					[problem(funzionalita,_),
		             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		             risposta(_,_,problem(minuti,D)), D >= 10,
					 risposta(_,_,problem(funzioni,CERT4)), CERT4 < 0.5]
					    ==>
					[assert(incident(sw)),
					 assert(dettaglio(serv)),
					 assert(soluzione(applicazione)),
					 assert(goal(solution)),
					 assert((problem(minuti,D))),
					 assert((problem(funzioni,CERT4)))]				
					explained_by [22,221]
					severity 3.
   
   		             rule 222:
					[problem(funzionalita,_),
		             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		             risposta(_,_,problem(durata_nel_tempo,intermittente)),
		             risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 0, VAL =< 5,
					 risposta(_,_,problem(funzioni,CERT4)), CERT4 < 0.5]
					    ==>
					[assert(incident(sw)),
					 assert(dettaglio(serv)),
					 assert(soluzione(stabilita)),
					 assert(goal(solution)),
					 assert((problem(durata_nel_tempo,intermittente))),
					 assert((problem(frequenza_problemi_analoghi,VAL))),
					 assert((problem(funzioni,CERT4)))]				
					explained_by [22,222]
					severity 3.
  
	rule 223:
	[problem(funzionalita,_),
	 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
	 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5,
	 risposta(_,_,problem(finalita,query))]
	==>
	[question_id(ID),
	 domanda(ID,info_scorrette,VAL,incert),
	 retract(certezza(C)),
	 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert((problem(piu_applicazioni_coinvolte,CERT2))),
	 assert((problem(piu_utenti_coinvolti,CERT3))),
	 assert((problem(finalita,query))),
	 assert(risposta(ID,incert,problem(info_scorrette,VAL)))]
	explained_by [223]
	severity 2.
		   
			         rule 2231:
					[problem(funzionalita,_),
					 problem(finalita,query),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,intermittente)),
			         risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
					 risposta(_,_,problem(info_scorrette,CERT4)), CERT4 > 0.5]
					    ==>
					[assert(incident(sw)),
					 assert(dettaglio(db)),
					 assert(soluzione(query)),
					 assert(goal(solution)),
					 retract(certezza(C)),
				     calcola_certezza([C,CERT4],0.8,NEWVAL),
				     assert(certezza(NEWVAL)),
					 assert((problem(durata_nel_tempo,intermittente))),
					 assert((problem(problemi_analoghi,CERT5))),
					 assert((problem(info_scorrette,CERT4)))]
					 explained_by [223,2231]
					 severity 2.
   
   			         rule 2232:
					[problem(funzionalita,_),
					 problem(finalita,query),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,intermittente)),
			         risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 10,
					 risposta(_,_,problem(info_scorrette,CERT4)), CERT4 > 0.5]
					    ==>
					[assert(incident(sw)),
					 assert(dettaglio(db)),
					 assert(soluzione(query_ripetuti)),
					 assert(goal(solution)),
					 retract(certezza(C)),
				     calcola_certezza([C,CERT4],0.8,NEWVAL),
				     assert(certezza(NEWVAL)),
					 assert((problem(durata_nel_tempo,intermittente))),
					 assert((problem(frequenza_problemi_analoghi,VAL))),
					 assert((problem(info_scorrette,CERT4)))]
					 explained_by [223,2232]
					 severity 2.
   
  		rule 224:
		[problem(funzionalita,_),
		 problem(finalita,query),
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		 risposta(_,_,problem(info_scorrette,CERT4)), CERT4 < 0.5]
		    ==>
		[question_id(ID),
		 domanda(ID,info_mancanti,VAL,incert),
		 assert(risposta(ID,incert,problem(info_mancanti,VAL)))]
		 explained_by [223]
		 severity 3.
   
   			         rule 2242:
					[problem(funzionalita,_),
					 problem(finalita,query),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					 risposta(_,_,problem(info_mancanti,CERT4)), CERT4 > 0.5]
					    ==>
					[assert(incident(hw)),
					 assert(dettaglio(disk)),
					 assert(soluzione(sostituzione)),
					 assert(goal(solution)),
					 retract(certezza(C)),
				     calcola_certezza([C,CERT4],0.8,NEWVAL),
				     assert(certezza(NEWVAL)),
					 assert((problem(info_mancanti,CERT4)))]
					 explained_by [223,2242]
					 severity 3.
   		    
   		       		  rule 2243:
					  [problem(funzionalita,_),
			           problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			           problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			           problem(finalita,query),
					   risposta(_,_,problem(info_mancanti,CERT4)), CERT4 < 0.5]
					  ==>
						[question_id(ID),
						 domanda(ID,disconnessione_utilizzo,VAL,incert),
						 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
						 explained_by [223]
						 severity 2.
   		    
rule 225:
[problem(funzionalita,_),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5,
 risposta(_,_,problem(finalita,mail))]
==>
[question_id(ID),
 domanda(ID,posta,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert((problem(finalita,mail))),
 assert(risposta(ID,incert,problem(posta,VAL)))]
 explained_by [225]
 severity 2.
		   
			         rule 2251:
					[problem(funzionalita,_),
					 problem(finalita,mail),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,prolungata)),
			         risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
					 risposta(_,_,problem(posta,CERT4)), CERT4 > 0.5]
					    ==>
					[assert(incident(sw)),
					 assert(dettaglio(serv)),
					 assert(soluzione(smtp)),
					 assert(goal(solution)),
					 retract(certezza(C)),
				     calcola_certezza([C,CERT4,CERT5],0.8,NEWVAL),
				     assert(certezza(NEWVAL)),
					 assert((problem(durata_nel_tempo,prolungata))),
					 assert((problem(posta,CERT4))),
					 assert((problem(problemi_analoghi,CERT5)))]
					 explained_by [225,2251]
					 severity 2.
   
		   		    rule 2552:
					[problem(funzionalita,_),
					 problem(finalita,mail),
		             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					 risposta(_,_,problem(posta,CERT4)), CERT4 < 0.5]
					    ==>
					[question_id(ID),
					 domanda(ID,disconnessione_utilizzo,VAL,incert),
					 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
					 explained_by [225]
					 severity 2.
   
   
rule 226:
[problem(funzionalita,_),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5,
 risposta(_,_,problem(finalita,report))]
 ==>
[question_id(ID),
 domanda(ID,visibilita,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert((problem(finalita,report))),
 assert(risposta(ID,incert,problem(visibilita,VAL)))]
explained_by [226]
severity 3.
   		    
   		            rule 2261:
					[problem(funzionalita,_),
					 problem(finalita,report),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			         risposta(_,_,problem(minuti,D)), D >= 10,
					 risposta(_,_,problem(visibilita,CERT4)), CERT4 < 0.5]
					    ==>
					[assert(incident(hw)),
					 assert(dettaglio(disk)),
					 assert(soluzione(reset)),
					 assert(goal(solution)),
					 assert((problem(minuti,D))),
					 assert((problem(visibilita,CERT4)))]
					 explained_by [226,2261]
					 severity 3.
   		      
   		         	rule 2262:
					[problem(funzionalita,_),
					 problem(finalita,report),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,intermittente)),
			         risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5,
					 risposta(_,_,problem(visibilita,CERT4)), CERT4 < 0.5]
					    ==>
					[assert(incident(hw)),
					 assert(dettaglio(disk)),
					 assert(soluzione(sostituzione)),
					 assert(goal(solution)),
					 assert((problem(frequenza_problemi_analoghi,VAL))),
					 assert((problem(durata_nel_tempo,intermittente))),
					 assert((problem(visibilita,CERT4)))]
					 explained_by [226,2262]
					 severity 3.
					 
				   rule 2263:
					[problem(funzionalita,_),
					 problem(finalita,report),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			         risposta(_,_,problem(durata_nel_tempo,intermittente)),
			         risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), 
			         VAL >= 0, VAL < 5, 
					 risposta(_,_,problem(visibilita,CERT4)), CERT4 < 0.5]
					    ==>
					[assert(incident(sw)),
					 assert(dettaglio(serv)),
					 assert(soluzione(refresh)),
					 assert(goal(solution)),
					 assert((problem(frequenza_problemi_analoghi,VAL))),
					 assert((problem(durata_nel_tempo,intermittente))),
					 assert((problem(visibilita,CERT4)))]
					 explained_by [226,2263]
					 severity 3.
				 
				 	rule 2264:
					[problem(funzionalita,_),
					 problem(finalita,report),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					 risposta(_,_,problem(visibilita,CERT4)), CERT4 > 0.5]
					    ==>
					[question_id(ID),
					 domanda(ID,doc,VAL,incert),
					 retract(certezza(C)),
					 calcola_certezza([C,CERT4],0.8,NEWVAL),
					 assert(certezza(NEWVAL)),
					 assert(risposta(ID,incert,problem(doc,VAL)))]
					 explained_by [226]
					 severity 3.
			
					   		rule 22641:
							[problem(funzionalita,_),
							 problem(finalita,report),
					         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
					         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					         risposta(_,_,problem(minuti,D)), D >= 10,
							 risposta(_,_,problem(doc,CERT4)), CERT4 < 0.5]
							    ==>
							[assert(incident(sw)),
					         assert(dettaglio(serv)),
					         assert(soluzione(corruzionefs)),
					         assert(goal(solution)),
							 assert((problem(minuti,D))),
					         assert((problem(doc,CERT4)))]
							 explained_by [226,22641]
							 severity 3.	 
						 
						 	rule 22642:
							[problem(funzionalita,_),
							 problem(finalita,report),
					         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
					         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					         risposta(_,_,problem(durata_nel_tempo,intermittente)),
							 risposta(_,_,problem(doc,CERT4)), CERT4 < 0.5]
							    ==>
							[assert(incident(sw)),
					         assert(dettaglio(serv)),
					         assert(soluzione(refresh)),
					         assert(goal(solution)),
							 assert((problem(durata_nel_tempo,intermittente))),
					         assert((problem(doc,CERT4)))]
							 explained_by [226,22642]
							 severity 2.	 
						 
						 	rule 22643:
							[problem(funzionalita,_),
							 problem(finalita,report),
					         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
					         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							 risposta(_,_,problem(doc,CERT4)), CERT4 > 0.5]
							    ==>
							[question_id(ID),
							 domanda(ID,transfer,VAL,incert),
							 retract(certezza(C)),
							 calcola_certezza([C,CERT4],0.8,NEWVAL),
							 assert(certezza(NEWVAL)),
							 assert(risposta(ID,incert,problem(transfer,VAL)))]
							 explained_by [226]
							 severity 2.
		   		    
							    rule 226431:
								[problem(funzionalita,_),
								 problem(finalita,report),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
								 risposta(_,_,problem(transfer,CERT4)), CERT4 > 0.5]
								    ==>
								[assert(incident(sw)),
						         assert(dettaglio(serv)),
						         assert(soluzione(ftp)),
						         assert(goal(solution)),
						         retract(certezza(C)),
							     calcola_certezza([C,CERT4],0.8,NEWVAL),
							     assert(certezza(NEWVAL)),
						         assert((problem(transfer,CERT4)))]
								 explained_by [226,226431]
								 severity 2.
						 
			   		           rule 226432:
								[problem(funzionalita,_),
								 problem(finalita,report),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
								 risposta(_,_,problem(transfer,CERT4)), CERT4 < 0.5]
								    ==>
								[question_id(ID),
								 domanda(ID,disconnessione_utilizzo,VAL,incert),
								 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
								 explained_by [226]
								 severity 2.
		   		    
   		    
    rule 227:
	[problem(funzionalita,_),
     problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
     problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
	 risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 < 0.5]
	    ==>
	[question_id(ID),
	 domanda(ID,lentezza_server,VAL,incert),
	 assert(risposta(ID,incert,problem(lentezza_server,VAL)))]
     explained_by [22]
     severity 3.
					
						    rule 2271:
							[problem(funzionalita,_),
			                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
							 risposta(_,_,problem(minuti,D)), D >= 0, D < 10]
							  ==>
							[assert(incident(sovraccarico)),
							 assert(dettaglio(serv)),
							 assert(soluzione(processi)),
							 assert(goal(solution)),
							 retract(certezza(C)),
							 calcola_certezza([C,CERT4],0.8,NEWVAL),
							 assert(certezza(NEWVAL)),
						     assert((problem(lentezza_server,CERT4))),
						     assert((problem(minuti,D)))]					
							explained_by [22,2271]
							severity 3.
	
							rule 2272:
							[problem(funzionalita,_),
			                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
							 risposta(_,_,problem(durata_nel_tempo,intermittente)),
							 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 0, VAL < 5]
							==>
							[assert(incident(hw)),
							 assert(dettaglio(serv)),
							 assert(soluzione(upgrade)),
							 assert(goal(solution)),
							 retract(certezza(C)),
							 calcola_certezza([C,CERT4],0.8,NEWVAL),
							 assert(certezza(NEWVAL)),
							 assert((problem(durata_nel_tempo,intermittente))),
							 assert((problem(lentezza_server,CERT4))),
							 assert((problem(frequenza_problemi_analoghi,VAL)))]					
							 explained_by [22,2272]
							 severity 3.
	    
							rule 2273:
							[problem(funzionalita,_),
			                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
			                 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
			                 risposta(_,_,problem(durata_nel_tempo,intermittente)),
							 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5]
							==>
							[assert(incident(hw)),
							 assert(dettaglio(cpuram)),
							 assert(soluzione(sostituzione)),
							 assert(goal(solution)),
							 retract(certezza(C)),
							 calcola_certezza([C,CERT4],0.8,NEWVAL),
							 assert(certezza(NEWVAL)),
							 assert((problem(durata_nel_tempo,intermittente))),
							 assert((problem(lentezza_server,CERT4))),
							 assert((problem(frequenza_problemi_analoghi,VAL)))]					
							 explained_by [22,2273]
							 severity 3.     
			   
			   				rule 2274:
							[problem(funzionalita,_),
			                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
							 risposta(_,_,problem(minuti,D)), D >= 10]
							    ==>
							[question_id(ID),
							 domanda(ID,blocco,VAL,incert),
							 retract(certezza(C)),
							 calcola_certezza([C,CERT4],0.8,NEWVAL),
							 assert(certezza(NEWVAL)),
							 assert(risposta(ID,incert,problem(blocco,VAL))),
							 assert((problem(lentezza_server,CERT4))),
							 assert((problem(minuti,D)))]					
							 explained_by [22,2274]
							 severity 3.
		   
						   			rule 27741:
							        [problem(funzionalita,_),
			                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							         problem(lentezza_server,CERT4), CERT4 > 0.5,
									 risposta(_,_,problem(blocco,CERT5)), CERT5 > 0.5]
										==>
								    [assert(incident(crash)),
								     assert(dettaglio(hang)),
								     assert(soluzione(serv)),
								     assert(goal(solution)),
							         assert((problem(blocco,CERT5)))]					
							        explained_by [22,2274,27741]
							        severity 3.
								
			   						rule 27742:
							        [problem(funzionalita,_),
			                         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							         problem(lentezza_server,CERT4), CERT4 > 0.5,
							         risposta(_,_,problem(finalita,query)),
									 risposta(_,_,problem(blocco,CERT5)), CERT5 > 0.5]
										==>
								    [assert(incident(crash)),
								     assert(dettaglio(hang)),
								     assert(soluzione(db)),
								     assert(goal(solution)),
							         assert((problem(finalita,query))),
							         assert((problem(blocco,CERT5)))]					
							        explained_by [22,2274,27742]
							        severity 3.
			   
			   			   	rule 2275:
							[problem(funzionalita,_),
			                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
							 risposta(_,_,problem(durata_nel_tempo,intermittente)),
							 risposta(_,_,problem(finalita,mail))]
							    ==>
								[assert(incident(sovraccarico)),
								 assert(dettaglio(serv)),
								 assert(soluzione(mail)),
								 assert(goal(solution)),
								 retract(certezza(C)),
							     calcola_certezza([C,CERT4],0.8,NEWVAL),
							     assert(certezza(NEWVAL)),
							     assert((problem(lentezza_server,CERT4))),
							     assert((problem(durata_nel_tempo,intermittente))),
							     assert((problem(finalita,mail)))]					
							explained_by [22,2275]
							severity 3.
							
							rule 2276:
							[problem(funzionalita,_),
			                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
			                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
							 risposta(_,_,problem(lentezza_server,CERT4)), CERT4 > 0.5,
							 risposta(_,_,problem(durata_nel_tempo,intermittente)),
							 risposta(_,_,problem(finalita,query))]
							    ==>
								[assert(incident(sovraccarico)),
								 assert(dettaglio(serv)),
								 assert(soluzione(db)),
								 assert(goal(solution)),
								 retract(certezza(C)),
							     calcola_certezza([C,CERT4],0.8,NEWVAL),
							     assert(certezza(NEWVAL)),
							     assert((problem(lentezza_server,CERT4))),
							     assert((problem(durata_nel_tempo,intermittente))),
							     assert((problem(finalita,query)))]					
							explained_by [22,2276]
							severity 3.
			   
			   
		rule 228:
		[problem(funzionalita,_),
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
         risposta(_,_,problem(durata_nel_tempo,intermittente)),
		 risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 > 0.5,
		 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 0, VAL < 5]
		==>
		[assert(incident(hw)),
		 assert(dettaglio(serv)),
		 assert(soluzione(upgrade)),
		 assert(goal(solution)),
		 retract(certezza(C)),
		 calcola_certezza([C,CERT4],0.8,NEWVAL),
		 assert(certezza(NEWVAL)),
		 assert((problem(durata_nel_tempo,intermittente))),
		 assert((problem(disconnessione_utilizzo,CERT4))),
		 assert((problem(frequenza_problemi_analoghi,VAL)))]					
		 explained_by [22,228]
		 severity 3.
						
		rule 2281:
		[problem(funzionalita,_),
         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
         risposta(_,_,problem(durata_nel_tempo,intermittente)),
         risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 > 0.5,
		 risposta(_,_,problem(frequenza_problemi_analoghi,VAL)), VAL >= 5]
		==>
		[assert(incident(hw)),
		 assert(dettaglio(serv)),
		 assert(soluzione(sostituzione)),
		 assert(goal(solution)),
		 retract(certezza(C)),
		 calcola_certezza([C,CERT4],0.8,NEWVAL),
		 assert(certezza(NEWVAL)),
		 assert((problem(durata_nel_tempo,intermittente))),
		 assert((problem(disconnessione_utilizzo,CERT4))),
		 assert((problem(frequenza_problemi_analoghi,VAL)))]					
		 explained_by [22,2281]
		 severity 3.     
	   
   				rule 2282:
				[problem(funzionalita,_),
                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
                 risposta(_,_,problem(durata_nel_tempo,intermittente)),
                 risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 > 0.5,
				 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5]
				==>
				[question_id(ID),
				 domanda(ID,err_code,VAL,numeric),
				 assert((problem(durata_nel_tempo,intermittente))),
				 assert((problem(disconnessione_utilizzo,CERT4))),
				 assert((problem(problemi_analoghi,CERT5))),
				 retract(certezza(C)),
		         calcola_certezza([C,CERT4],0.8,NEWVAL),
		         assert(certezza(NEWVAL)),
				 assert(risposta(ID,incert,problem(err_code,VAL)))]
				 explained_by [22,2282]
				 severity 2.  
				               
						rule 22821:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 risposta(_,_,problem(err_code,VAL)), VAL == 404]
						==>
						[assert(incident(sw)),
		                 assert(dettaglio(db)),
		                 assert(soluzione(stabilita)),
		                 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
			             explained_by [28,2282,22821]
			             severity 2.

						rule 22822:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 risposta(_,_,problem(err_code,VAL)), VAL == 500]
						==>
						[assert(incident(sw)),
						 assert(dettaglio(serv)),
						 assert(soluzione(refresh)),
						 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
						 explained_by [28,2282,22822]
						 severity 2.			
		
						rule 22823:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 risposta(_,_,problem(err_code,VAL)), 
						 VAL == 502]
						 ==>
						[assert(incident(sovraccarico)),
						 assert(dettaglio(serv)),
						 assert(soluzione(processi)),
						 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
					     explained_by [28,2282,22823]
					     severity 2.	
					    
					    rule 22824:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 risposta(_,_,problem(err_code,VAL)), 
						 VAL == 403]
						 ==>
						[assert(incident(security)),
						 assert(dettaglio(firewall)),
						 assert(soluzione(risorse)),
						 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
					     explained_by [28,2282,22824]
					     severity 2.	
					    
					    rule 22825:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 risposta(_,_,problem(err_code,VAL)), 
						 VAL == 401]
						 ==>
						[assert(incident(security)),
						 assert(dettaglio(firewall)),
						 assert(soluzione(risorse)),
						 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
					     explained_by [28,2282,22825]
					     severity 2.	
					    
					    rule 22826:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 risposta(_,_,problem(err_code,VAL)), 
						 VAL == 400]
						 ==>
						[assert(incident(err_generico)),
						 assert(dettaglio(client)),
						 assert(soluzione(sintassi)),
						 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
					     explained_by [28,2282,22826]
					     severity 2.	
	 

   rule 229:
	[problem(funzionalita,_),
     risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
     risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5,
     risposta(_,_,problem(finalita,altro))]
	    ==>
	[question_id(ID),
	 domanda(ID,stampa,VAL,incert),
	 retract(certezza(C)),
	 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
	 assert(certezza(NEWVAL)),
	 assert((problem(piu_applicazioni_coinvolte,CERT2))),
     assert((problem(piu_utenti_coinvolti,CERT3))),
	 assert((problem(finalita,altro))),
	 assert(risposta(ID,incert,problem(stampa,VAL)))]
     explained_by [229]
     severity 2.

							    rule 2291:
								[problem(funzionalita,_),
								 problem(finalita,altro),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
								 risposta(_,_,problem(stampa,CERT4)), CERT4 > 0.5]
								    ==>
								[assert(incident(sw)),
						         assert(dettaglio(serv)),
						         assert(soluzione(stampa)),
						         assert(goal(solution)),
						         retract(certezza(C)),
		                         calcola_certezza([C,CERT4],0.8,NEWVAL),
		                         assert(certezza(NEWVAL)),
						         assert((problem(stampa,CERT4)))]
								 explained_by [229,2291]
								 severity 2.

							    rule 2292:
								[problem(funzionalita,_),
								 problem(finalita,altro),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
								 risposta(_,_,problem(stampa,CERT4)), CERT4 < 0.5]
								    ==>
								[question_id(ID),
					             domanda(ID,orario,VAL,incert),
						         assert(risposta(ID,incert,problem(orario,VAL)))]
								 explained_by [229]
								 severity 2.
                    
		                                rule 22921:
										[problem(funzionalita,_),
										 problem(finalita,altro),
								         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
								         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
										 risposta(_,_,problem(orario,CERT4)), CERT4 < 0.5]
										    ==>
										[assert(incident(sw)),
								         assert(dettaglio(serv)),
								         assert(soluzione(ntp)),
								         assert(goal(solution)),
								         retract(certezza(C)),
		                                 calcola_certezza([C,CERT4],0.8,NEWVAL),
		                                 assert(certezza(NEWVAL)),
								         assert((problem(orario,CERT4)))]
										 explained_by [229,22921]
										 severity 2.

                                rule 22922:
								[problem(funzionalita,_),
								 problem(finalita,altro),
						         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
						         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
								 risposta(_,_,problem(orario,CERT4)), CERT4 > 0.5]
								    ==>
								[question_id(ID),
					             domanda(ID,schedulazioni,VAL,incert),
					             retract(certezza(C)),
					             calcola_certezza([C,CERT4],0.8,NEWVAL),
					             assert(certezza(NEWVAL)),
						         assert(risposta(ID,incert,problem(schedulazioni,VAL)))]
								 explained_by [229]
								 severity 2.

		 		                                rule 229221:
												[problem(funzionalita,_),
												 problem(finalita,altro),
										         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
										         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
										         risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
												 risposta(_,_,problem(schedulazioni,CERT4)), CERT4 > 0.5]
												    ==>
												[assert(incident(sw)),
										         assert(dettaglio(serv)),
										         assert(soluzione(cron)),
										         assert(goal(solution)),
										         retract(certezza(C)),
							                     calcola_certezza([C,CERT4,CERT5],0.8,NEWVAL),
							                     assert(certezza(NEWVAL)),
							                     assert((problem(problemi_analoghi,CERT5))),
										         assert((problem(schedulazioni,CERT4)))]
												 explained_by [229,229221]
												 severity 2.

				                            	 rule 229222:
												[problem(funzionalita,_),
												 problem(finalita,altro),
										         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
										         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
												 risposta(_,_,problem(schedulazioni,CERT4)), CERT4 < 0.5]
												    ==>
												[question_id(ID),
												 domanda(ID,disconnessione_utilizzo,VAL,incert),
												 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
												 explained_by [229,229222]
												 severity 2.

 
rule 23:
[problem(funzionalita,_),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5,
 risposta(_,_,problem(durata_nel_tempo,intermittente))]
==>
[question_id(ID),
 domanda(ID,disconnessione_utilizzo,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert((problem(durata_nel_tempo,intermittente))),
 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
 explained_by [23]
 severity 2.

   			rule 231:
			[problem(funzionalita,_),
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
             problem(durata_nel_tempo,intermittente),
             risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 > 0.5,
			 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5]
			==>
			[question_id(ID),
			 domanda(ID,err_code,VAL,numeric),
			 retract(certezza(C)),
			 calcola_certezza([C,CERT4],0.8,NEWVAL),
			 assert(certezza(NEWVAL)),
			 assert((problem(disconnessione_utilizzo,CERT4))),
			 assert((problem(problemi_analoghi,CERT5))),
			 assert(risposta(ID,incert,problem(err_code,VAL)))]
			 explained_by [23,231]
			 severity 2.  
               
						rule 2311:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 risposta(_,_,problem(err_code,VAL)), VAL == 400]
						==>
						[assert(incident(err_generico)),
		                 assert(dettaglio(client)),
		                 assert(soluzione(sintassi)),
		                 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
			             explained_by [23,231,2311]
			             severity 2.

						rule 2312:
						[problem(funzionalita,_),
		                 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		                 problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		                 problem(durata_nel_tempo,intermittente),
		                 problem(problemi_analoghi,CERT5), CERT5 < 0.5,
		                 problem(disconnessione_utilizzo,CERT4), CERT4 > 0.5,
		                 risposta(_,_,problem(err_code,VAL)), VAL == 403]
						==>
						[assert(incident(security)),
						 assert(dettaglio(firewall)),
						 assert(soluzione(risorse)),
						 assert(goal(solution)),
		                 assert((problem(err_code,VAL)))]
						 explained_by [23,231,2312]
						 severity 2.			
		
   			rule 232:
			[problem(funzionalita,_),
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
             problem(durata_nel_tempo,intermittente),
             risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 < 0.5,
			 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5]
			==>
			[assert(incident(sovraccarico)),
			 assert(dettaglio(client)),
			 assert(soluzione(utilizzo)),
			 assert(goal(solution)),
			 retract(certezza(C)),
             calcola_certezza([C,CERT4,CERT5],0.8,NEWVAL),
             assert(certezza(NEWVAL)),
			 assert((problem(disconnessione_utilizzo,CERT4))),
			 assert((problem(problemi_analoghi,CERT5)))]
			 explained_by [23,232]
			 severity 2.  

          
          
rule 24:
[problem(funzionalita,_),
 risposta(_,_,problem(finalita,report)),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
    ==>
[question_id(ID),
 domanda(ID,visibilita,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(finalita,report))),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(visibilita,VAL)))]
 explained_by [24]
 severity 2.

   		rule 241:
		[problem(funzionalita,_),
		 problem(finalita,report),
		 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
		 risposta(_,_,problem(frequenza_utilizzo,bassa)),
		 risposta(_,_,problem(visibilita,CERT4)), CERT4 < 0.5]
		 ==>
		[assert(incident(err_generico)),
		 assert(dettaglio(client)),
		 assert(soluzione(files)),
		 assert(goal(solution)),
		 retract(certezza(C)),
         calcola_certezza([C,CERT4],0.8,NEWVAL),
         assert(certezza(NEWVAL)),
		 assert((problem(visibilita,CERT4))),
		 assert((problem(frequenza_utilizzo,bassa)))]
		 explained_by [24,241]
		 severity 1.	 
		 
		 rule 242:
		[problem(funzionalita,_),
		 problem(finalita,report),
		 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
		 risposta(_,_,problem(visibilita,CERT4)), CERT4 > 0.5]
		 ==>
		[question_id(ID),
		 domanda(ID,doc,VAL,incert),
		 retract(certezza(C)),
		 calcola_certezza([C,CERT4],0.8,NEWVAL),
		 assert(certezza(NEWVAL)),
		 assert(risposta(ID,incert,problem(doc,VAL)))]
		 explained_by [24]
		 severity 1.	 

     			rule 2421:
				[problem(funzionalita,_),
				 problem(finalita,report),
		         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(doc,CERT4)), CERT4 < 0.5]
				    ==>
				[assert(incident(security)),
		         assert(dettaglio(firewall)),
		         assert(soluzione(risorse)),
		         assert(goal(solution)),
		         assert((problem(doc,CERT4)))]
				 explained_by [24,2421]
				 severity 3.	

     			rule 2422:
				[problem(funzionalita,_),
				 problem(finalita,report),
		         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(doc,CERT4)), CERT4 > 0.5]
				    ==>
				[question_id(ID),
				 domanda(ID,transfer,VAL,incert),
				 retract(certezza(C)),
				 calcola_certezza([C,CERT4],0.8,NEWVAL),
				 assert(certezza(NEWVAL)),
				 assert(risposta(ID,incert,problem(transfer,VAL)))]
				 explained_by [24]
				 severity 1.	

		                rule 242221:
						[problem(funzionalita,_),
						 problem(finalita,report),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				         risposta(_,_,problem(frequenza_utilizzo,alta)),
						 risposta(_,_,problem(transfer,CERT4)), CERT4 > 0.5]
						    ==>
						[assert(incident(sw)),
				         assert(dettaglio(client)),
				         assert(soluzione(spaziofull)),
				         assert(goal(solution)),
				         retract(certezza(C)),
		                 calcola_certezza([C,CERT4],0.8,NEWVAL),
		                 assert(certezza(NEWVAL)),
				         assert((problem(frequenza_utilizzo,alta))),
				         assert((problem(transfer,CERT4)))]
						 explained_by [24,242221]
						 severity 3.	

		                rule 242222:
						[problem(funzionalita,_),
						 problem(finalita,report),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(transfer,CERT4)), CERT4 < 0.5]
						    ==>
						[question_id(ID),
						 domanda(ID,disconnessione_utilizzo,VAL,incert),
						 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
						 explained_by [24]
						 severity 3.


rule 240:
[problem(funzionalita,_),
 risposta(_,_,problem(finalita,altro)),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
    ==>
[question_id(ID),
 domanda(ID,stampa,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(finalita,altro))),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(stampa,VAL)))]
 explained_by [240]
 severity 2.
		 
				rule 2401:
				[problem(funzionalita,_),
				 problem(finalita,report),
				 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(stampa,CERT4)), CERT4 > 0.5]
				 ==>
				[assert(incident(sw)),
				 assert(dettaglio(client)),
				 assert(soluzione(stampa)),
				 assert(goal(solution)),
				 retract(certezza(C)),
                 calcola_certezza([C,CERT4],0.8,NEWVAL),
                 assert(certezza(NEWVAL)),
				 assert((problem(stampa,CERT4)))]
				 explained_by [240,2401]
				 severity 1.	  

                rule 2402:
				[problem(funzionalita,_),
				 problem(finalita,report),
		         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(stampa,CERT4)), CERT4 < 0.5]
				    ==>
				[question_id(ID),
				 domanda(ID,disconnessione_utilizzo,VAL,incert),
				 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
				 explained_by [240]
				 severity 3.
			
		
				 
rule 2400:
[problem(funzionalita,_),
 risposta(_,_,problem(finalita,mail)),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 < 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 < 0.5]
    ==>
[question_id(ID),
 domanda(ID,posta,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(finalita,mail))),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(posta,VAL)))]
 explained_by [2400]
 severity 2.
		 
				rule 24001:
				[problem(funzionalita,_),
				 problem(finalita,mail),
				 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(durata_nel_tempo,prolungata)),
				 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
				 risposta(_,_,problem(posta,CERT4)), CERT4 > 0.5]
				 ==>
				[assert(incident(err_generico)),
				 assert(dettaglio(client)),
				 assert(soluzione(posta)),
				 assert(goal(solution)),
				 retract(certezza(C)),
		         calcola_certezza([C,CERT4,CERT5],0.8,NEWVAL),
		         assert(certezza(NEWVAL)),
				 assert((problem(durata_nel_tempo,prolungata))),
				 assert((problem(problemi_analoghi,CERT5))),
				 assert((problem(posta,CERT4)))]
				 explained_by [2400,24001]
				 severity 1.	  

				rule 24002:
				[problem(funzionalita,_),
				 problem(finalita,mail),
				 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(durata_nel_tempo,intermittente)),
				 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5,
				 risposta(_,_,problem(posta,CERT4)), CERT4 > 0.5]
				 ==>
				[assert(incident(network)),
				 assert(dettaglio(temporaneo)),
				 assert(soluzione(refresh)),
				 assert(goal(solution)),
				 retract(certezza(C)),
		         calcola_certezza([C,CERT4,CERT5],0.8,NEWVAL),
		         assert(certezza(NEWVAL)),
				 assert((problem(durata_nel_tempo,intermittente))),
				 assert((problem(problemi_analoghi,CERT5))),
				 assert((problem(posta,CERT4)))]
				 explained_by [2400,24002]
				 severity 1.	  

                rule 24003:
				[problem(funzionalita,_),
				 problem(finalita,mail),
		         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
		         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
				 risposta(_,_,problem(posta,CERT4)), CERT4 > 0.5]
				    ==>
				[question_id(ID),
				 domanda(ID,allegati,VAL,incert),
				 assert(risposta(ID,incert,problem(allegati,VAL)))]
				 explained_by [2400]
				 severity 3.

						rule 240031:
						[problem(funzionalita,_),
						 problem(finalita,mail),
						 problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
						 problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(allegati,CERT4)), CERT4 > 0.5]
						 ==>
						[assert(incident(err_generico)),
						 assert(dettaglio(client)),
						 assert(soluzione(allegati)),
						 assert(goal(solution)),
						 retract(certezza(C)),
		                 calcola_certezza([C,CERT4],0.8,NEWVAL),
		                 assert(certezza(NEWVAL)),
						 assert((problem(allegati,CERT4)))]
						 explained_by [2400,240031]
						 severity 1.	  

		                rule 240032:
						[problem(funzionalita,_),
						 problem(finalita,mail),
				         problem(piu_applicazioni_coinvolte,CERT2), CERT2 < 0.5,
				         problem(piu_utenti_coinvolti,CERT3), CERT3 < 0.5,
						 risposta(_,_,problem(allegati,CERT4)), CERT4 < 0.5]
						    ==>
						[question_id(ID),
						 domanda(ID,disconnessione_utilizzo,VAL,incert),
						 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
						 explained_by [2400]
						 severity 3.
				 
	
			 

rule 25:
[problem(funzionalita,_),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5]
==>
[question_id(ID),
 domanda(ID,disconnessione_utilizzo,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(piu_applicazioni_coinvolte,CERT2))),
 assert((problem(piu_utenti_coinvolti,CERT3))),
 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
 explained_by [25]
 severity 2.
	  	
	  	   	rule 251:
			[problem(funzionalita,_),
             problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
             risposta(_,_,problem(durata_nel_tempo,intermittente)),
             risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 > 0.5,
			 risposta(_,_,problem(problemi_analoghi,CERT5)), CERT5 < 0.5]
			==>
			[assert(incident(network)),
			 assert(dettaglio(temporaneo)),
			 assert(soluzione(refresh)),
			 assert(goal(solution)),
			 retract(certezza(C)),
		     calcola_certezza([C,CERT4,CERT5],0.8,NEWVAL),
		     assert(certezza(NEWVAL)),
			 assert((problem(disconnessione_utilizzo,CERT4))),
			 assert((problem(durata_nel_tempo,intermittente))),
			 assert((problem(problemi_analoghi,CERT5)))]
			 explained_by [25,251]
			 severity 2.  
	  	
		  	rule 252:
			[problem(funzionalita,_),
	         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
	         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
	         risposta(_,_,problem(disconnessione_utilizzo,CERT4)), CERT4 < 0.5]
			==>
			[question_id(ID),
			 domanda(ID,lentezza_rete,VAL,incert),
			 retract(certezza(C)),
			 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
			 assert(certezza(NEWVAL)),
			 assert(risposta(ID,incert,problem(lentezza_rete,VAL)))]
			 explained_by [25]
			 severity 2.
	  	
			  		rule 2521:
					[problem(funzionalita,_),
		             problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
		             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		             risposta(_,_,problem(lentezza_rete,CERT4)), CERT4 > 0.5,
		             risposta(_,_,problem(minuti,FREQ)), FREQ >= 0, FREQ < 10]
					==>
					[assert(incident(sovraccarico)),
					 assert(dettaglio(network)),
					 assert(soluzione(traffico)),
					 assert(goal(solution)),
					 retract(certezza(C)),
		             calcola_certezza([C,CERT4],0.8,NEWVAL),
		             assert(certezza(NEWVAL)),
					 assert((problem(lentezza_rete,CERT4))),
					 assert((problem(minuti,FREQ)))]
					 explained_by [25,252,2521]
					 severity 2.  
					 
					rule 2522:
					[problem(funzionalita,_),
		             problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
		             problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
		             risposta(_,_,problem(lentezza_rete,CERT4)), CERT4 > 0.5,
		             risposta(_,_,problem(minuti,FREQ)), FREQ >= 10]
					==>
					[assert(incident(crash)),
					 assert(dettaglio(down)),
					 assert(soluzione(network)),
					 assert(goal(solution)),
					 retract(certezza(C)),
		             calcola_certezza([C,CERT4],0.8,NEWVAL),
		             assert(certezza(NEWVAL)),
					 assert((problem(lentezza_rete,CERT4))),
					 assert((problem(minuti,FREQ)))]
					 explained_by [25,252,2522]
					 severity 2.  
	
	
rule 26:
[problem(funzionalita,_),
 risposta(_,_,problem(finalita,report)),
 risposta(_,_,problem(piu_applicazioni_coinvolte,CERT2)), CERT2 > 0.5,
 risposta(_,_,problem(piu_utenti_coinvolti,CERT3)), CERT3 > 0.5]
==>
[question_id(ID),
 domanda(ID,transfer,VAL,incert),
 retract(certezza(C)),
 calcola_certezza([C,CERT2,CERT3],0.8,NEWVAL),
 assert(certezza(NEWVAL)),
 assert((problem(finalita,report))),
 assert(risposta(ID,incert,problem(transfer,VAL)))]
 explained_by [26]
 severity 2.	

	                rule 261:
					[problem(funzionalita,_),
					 problem(finalita,report),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					 risposta(_,_,problem(transfer,CERT4)), CERT4 > 0.5]
					    ==>
					[assert(incident(network)),
			         assert(dettaglio(temporaneo)),
			         assert(soluzione(trasferimento)),
			         assert(goal(solution)),
			         retract(certezza(C)),
                     calcola_certezza([C,CERT4],0.8,NEWVAL),
                     assert(certezza(NEWVAL)),
			         assert((problem(transfer,CERT4)))]
					 explained_by [26,261]
					 severity 2.	
	
		            rule 262:
					[problem(funzionalita,_),
					 problem(finalita,report),
			         problem(piu_applicazioni_coinvolte,CERT2), CERT2 > 0.5,
			         problem(piu_utenti_coinvolti,CERT3), CERT3 > 0.5,
					 risposta(_,_,problem(transfer,CERT4)), CERT4 < 0.5]
					    ==>
					[question_id(ID),
					 domanda(ID,transfer,VAL,incert),
					 assert(risposta(ID,incert,problem(disconnessione_utilizzo,VAL)))]
					 explained_by [26]
					 severity 2.	
					 
					 
