% Module: soluzioni.pl
% --------
%
% Il modulo contiene le azioni consigliate sulla base degli incident 
% rilevati e dei relativi dettagli
% 
soluzione(err_generico,dispositivo,collegamento,"Il dispositivo di accesso non viene riconosciuto dal sistema. Si consiglia di controllare il corretto collegamento dello stesso al proprio PC").
soluzione(err_generico,dispositivo,stampa,"Problemi di comunicazione tra stampante e sistema. Si consiglia di controllare il corretto collegamento della stessa al proprio PC").
soluzione(err_generico,client,controlla_url,"La pagina richiesta non è più disponibile in quanto la URL è stata recentemente aggiornata. Controllare la correttezza dell'indirizzo inserito, e riprovare l'accesso").
soluzione(err_generico,client,connessione,"Il sistema si trova attualmente fuori rete. Controllare il corretto collegamento alla rete del proprio computer").
soluzione(err_generico,client,sintassi,"La richiesta è stata formulata in modo sbagliato e non può essere soddisfatta. Si consiglia di controllare se siano presenti o meno errori di sintassi").
soluzione(err_generico,client,files,"I documenti richiesti non risultano disponibili, ma potrebbero trovarsi in altre cartelle, o essere stati cancellati. Effettuare un controllo attraverso gli strumenti di ricerca del sistema").
soluzione(err_generico,client,mail,"I messaggi sono stati ricevuti correttamente, ma sono stati contrassegnati come \"posta indesiderata\". In caso di mancato invio, controllare se l'indirizzo mail di destinazione è corretto").
soluzione(err_generico,client,allegati,"I messaggi contengono allegati di dimensioni non consentite. Si consiglia di effettuare una compressione dei documenti attraverso un software ad hoc, e di riprovare l'invio ").

soluzione(security,user,creazione,"Nessuna user/password risulta associata alla tua utenza. In questo caso è necessario registrarsi al sistema ed attendere la successiva approvazione").
soluzione(security,user,reinsert,"User/password errate, si prega di effettuare un nuovo tentativo").
soluzione(security,user,dimenticata,"User/password errate, effettuare la procedura di recupero user/password attraverso l'apposito form").
soluzione(security,user,scaduta,"L'account associato alla tua utenza risulta temporaneamente sospeso, contattare il supporto SECURITY per eseguire la riattivazione").
soluzione(security,user,bloccata,"L'accesso al sistema è stato inibito a causa dei numerosi tentativi di login errati. Rivolgersi al supporto SECURITY per verificare le credenziali in tuo possesso ed effettuare l'eventuale sblocco").
soluzione(security,firewall,permessi,"Non si dispone dei privilegi necessari all'accesso applicativo. Verificare il ruolo associato all'utenza attraverso apertura di un ticket al supporto SECURITY").
soluzione(security,firewall,risorse,"Non si dispone dei privilegi necessari per l'accesso alla risorsa richiesta. Verificare il ruolo associato all'utenza apertura di un ticket al supporto SECURITY").
soluzione(security,firewall,antivirus,"Sul tuo PC sono presenti programmi antivirus che segnalano la pagina come potenziale fonte di pericolo. Controllare le impostazioni del software ed escludere tali pagine dalle eccezioni").
soluzione(security,malware,scansione,"All'interno del tuo PC sono presenti software indesiderati che causano instabilità durante l'utilizzo del sistema. Effettuare con urgenza una operazione di scansione files attraverso prodotti antivirus per la loro rimozione").
soluzione(security,malware,estensioni,"All'interno del browser risultano installate componenti non verificate, le quali provocano l'apertura di pagine indesiderate. Si consiglia di effettuare un'operazione di cleaning delle estensioni, o eventualmente la reinstallazione del browser").

soluzione(sw,client,impostazioni,"Il dispositivo personale viene riconosciuto dal sistema, ma si verificano errori durante il suo utilizzo. Si consiglia di verificare le impostazioni dell'applicazione con cui esso si interfaccia").
soluzione(sw,client,browser,"Problemi con l'utilizzo del browser web. La soluzione per questo problema è quella di cancellare i dati temporanei (ad esempio, cookie, cache, preferenze) e riprovare l'accesso. Se il problema si ripropone, si consiglia di reinstallare il software").
soluzione(sw,client,proxy,"L'accesso al sistema non è consentito a causa delle policy di sicurezza. Aggiungere la URL indicata alle eccezioni del proxy").
soluzione(sw,client,schede,"Il browser sta utilizzando troppe risorse sul tuo sistema. Si consiglia di chiudere alcune schede attualmente aperte ma non in uso").
soluzione(sw,client,connessione,"Si rilevano disconnessioni temporanee sul tuo sistema. Si consiglia di controllare le impostazioni di rete del sistema operativo. Se si è connessi in wifi il segnale potrebbe essere limitato").
soluzione(sw,client,applicazione,"Si rilevano anomalie sui software applicativi installati sul PC. Si consiglia di effettuare un controllo sulle impostazioni ed effettuare chiusura/apertura del programma. Se il problema dovesse persistere procedere alla reinstallazione").
soluzione(sw,client,trasferimento,"Sul tuo PC sono presenti programmi antivirus che bloccano il trasferimento segnalando i file come potenziale fonte di pericolo. Si può procedere al trasferimento rispondendo all'applicazione che il file indicato è sicuro").
soluzione(sw,client,spaziofull,"Il trasferimento dei documenti è interrotto dall'applicazione. Si richiede di controllare la grandezza del file e successivamente se ci sia spazio sufficiente sulla directory usata per il trasferimento").
soluzione(sw,client,stampa,"L'applicazione non è riuscita a trasferire i file da stampare su dispositivo. Trattasi di errore di natura temporanea, pertanto si richiede di tentare una nuova operazione di stampa").
soluzione(sw,client,ntp,"L'orario del proprio sistema operativo è disallineato, questo può causare problemi su alcune applicazioni. Si consiglia di procedere alla sincronizzazione attraverso le impostazioni di sistema").

soluzione(sw,serv,refresh,"Il sistema non è riuscito ad elaborare la richiesta, si consiglia di ricaricare la pagina e ripetere l'azione. Se il problema dovesse nuovamente ripresentarsi si consiglia apertura segnalazione al supporto APPLICATION per verifica lo stato dei servizi applicativi").
soluzione(sw,serv,stabilita,"Le componenti applicative installate sul server presentano instabilità, aprire una segnalazione attraverso ticket al supporto OPERATING_SYSTEM in quanto potrebbe essere necessario un upgrade del sistema").
soluzione(sw,serv,corruzionefs,"Impossibile effettuare operazioni a causa di errori di lettura/scrittura sul disco, aprire una segnalazione attraverso ticket al supporto OPERATING_SYSTEM per verifica di tipo software in quanto le partizioni che contengono i dati potrebbero essere corrotte").
soluzione(sw,serv,manutenzione,"Il sistema è temporaneamente indisponibile in quanto sono in corso attività programmate. Si consiglia di contattare attraverso ticket il supporto OPERATING_SYSTEM per verificare l'orario di chiusura di tali attività").
soluzione(sw,serv,applicazione,"Il sistema non carica correttamente alcune componenti applicative. Si consiglia di verificare lo stato dei servizi attraverso l'apertura di un ticket al supporto APPLICATION").
soluzione(sw,serv,smtp,"L'anomalia è dovuta alla mancata esecuzione di un servizio di sistema, il quale ha subito un arresto imprevisto. Si consiglia di aprire una segnalazione al supporto OPERATING_SYSTEM per verificare il corretto funzionamento del servizi applicativi di posta sul server").
soluzione(sw,serv,ftp,"L'anomalia è dovuta alla mancata esecuzione di un servizio di sistema, il quale ha subito un arresto imprevisto. Si consiglia di aprire una segnalazione al supporto OPERATING_SYSTEM per verificare il corretto funzionamento del servizio FTP sul server").
soluzione(sw,serv,stampa,"L'anomalia è dovuta alla mancata esecuzione di un servizio di sistema, il quale ha subito un arresto imprevisto. Si consiglia di aprire una segnalazione al supporto OPERATING_SYSTEM per verificare il corretto funzionamento del servizio di stampa sul server").
soluzione(sw,serv,ntp,"L'anomalia è dovuta alla mancata esecuzione di un servizio di sistema, il quale ha subito un arresto imprevisto. Si consiglia di aprire una segnalazione al supporto OPERATING_SYSTEM per verificare il corretto funzionamento del servizio orario NTP sul server").
soluzione(sw,serv,cron,"L'anomalia è dovuta alla mancata esecuzione di un servizio di sistema, il quale ha subito un arresto imprevisto. Si consiglia di aprire una segnalazione al supporto OPERATING_SYSTEM per verificare il corretto funzionamento del servizio orario NTP sul server").
soluzione(sw,network,instradamento,"Si stanno verificando problemi di connettività di rete, probabilmente dovuti a problemi di natura software. Contattare il supporto NETWORK per effettuare una verifica sull'instradamento degli apparati (router)").

soluzione(sw,db,refresh,"Rilevata mancata connettività temporanea al DB, si consiglia di ricaricare la pagina. Se il problema dovesse nuovamente ripresentarsi si consiglia apertura segnalazione al supporto DATABASE per verifica stato risorse db").
soluzione(sw,db,stabilita,"Rilevati problemi di comunicazione ripetuti tra sistema e db. Si richiede di far controllare con urgenza la stabilità del DB applicativo aprendo una segnalazione al supporto DATABASE").
soluzione(sw,db,query,"Rilevati errori nell'elaborazione della query da parte del DB. Si consiglia di riprovare ad eseguire la ricerca").
soluzione(sw,db,query_ripetuti,"Rilevati errori ripetuti nell'elaborazione della query da parte del DB. Si consiglia di far effettuare una verifica sulle tabelle del db aprendo una segnalazione ticket al supporto DATABASE").

soluzione(crash,down,serv,"Il sistema risulta indisponibile. E' necessario ripristinare il server aprendo una segnalazione tramite ticket al supporto OPERATING_SYSTEM").
soluzione(crash,down,db,"Il db risulta indisponibile. E' necessario ripristinare il database aprendo una segnalazione tramite ticket al supporto DATABASE").
soluzione(crash,down,network,"L'infrastruttura di rete non funziona correttamente o è indisponibile. E' necessario aprire una segnalazione al supporto NETWORK per verifica presenza di guasti su apparati di rete").
soluzione(crash,hang,serv,"Il sistema non risponde ai comandi, è necessario contattare il supporto OPERATING_SYSTEM tramite ticket per il suo ripristino").
soluzione(crash,hang,client,"Il pc non risponde ai comandi, è necessario effettuare un riavvio per procedere al suo ripristino").
soluzione(crash,hang,db,"Il database applicativo non risponde ai comandi, è necessario aprire una segnalazione al supporto DATABASE per il suo ripristino").

soluzione(hw,client,upgrade,"Il pc sta utilizzando un quantitativo non sufficiente di risorse hardware. Si consiglia di effettuare un aggiornamento delle componenti, o di procedere ad un upgrade RAM (se possibile)").
soluzione(hw,dispositivo,sostituzione,"Il dispositivo personale presenta problemi di natura hardware. Si consiglia l'apertura di una richiesta di sostituzione attraverso un ticket").
soluzione(hw,cpuram,sostituzione,"Sono stati rilevati problemi di natura hardware sul sistema. Si consiglia l'apertura di una richiesta di verifica al supporto OPERATING_SYSTEM per la verifica sullo stato di integrità delle componenti ram-cpu").
soluzione(hw,serv,upgrade,"Il sistema sta lavorando con un quantitativo non sufficiente di risorse hardware. Si consiglia l'apertura di una richiesta di verifica al supporto OPERATING_SYSTEM per valutare la possibiltà di eventuale upgrade componenti hardware").
soluzione(hw,disk,sostituzione,"Sono stati rilevati problemi di natura hardware sui dischi di sistema. Si consiglia l'apertura di una richiesta al supporto OPERATING SYSTEM per una verifica di tipo hardware sullo stato delle partizioni ed eventuale loro sostituzione (necessario backup)").
soluzione(hw,disk,reset,"I dischi di sistema risultano indisponibili. Si consiglia l'apertura di una richiesta al supporto OPERATING SYSTEM per il ripristino delle partizioni").
soluzione(hw,serv,sostituzione,"Sono stati rilevati problemi di natura hardware sul sistema. Si consiglia l'apertura di una richiesta al supporto OPERATING SYSTEM per la verifica di tipo hardware sullo stato delle componenti").
soluzione(hw,network,apparati,"Si stanno verificando ripetute perdite di connettività di rete, probabilmente dovuti a problemi di natura hardware sulle infrastutture di rete. Contattare il supporto NETWORK per effettuare una verifica di tipo hardware sullo stato degli apparati (router)").


soluzione(sovraccarico,client,schede,"L'anomalia è dovuta ad un utilizzo intensivo delle risorse da parte del browser. Si consiglia di chiudere eventuali schede aperte ma non utilizzate").
soluzione(sovraccarico,client,applicazioni,"L'anomalia è dovuta ad un utilizzo intensivo delle risorse del tuo pc. Si consiglia di chiudere applicazioni aperte ma non utilizzate").
soluzione(sovraccarico,client,utilizzo,"L'applicazione sta presentando difficoltà nell'elaborazione della richiesta a causa di un temporaneo utilizzo elevato da parte delle risorse del tuo pc. Se il problema dovesse ripresentarsi verificare la quantità di applicazioni in uso sul proprio pc ed eventualmente chiudere quelle che occupano più risorse").
soluzione(sovraccarico,serv,elaborazione,"L'applicazione sta presentando difficoltà nell'elaborazione della richiesta a causa di un carico di lavoro eccessivo sul server. Si consiglia l'apertura preliminare di una segnalazione al supporto OPERATING_SYSTEM per verificare lo stato dell'applicazione ").
soluzione(sovraccarico,serv,processi,"L'applicazione sta presentando difficoltà nell'elaborazione della richiesta a causa di un utilizzo intensivo da parte di alcuni processi di sistema. Si consiglia di aprire una segnalazione al supporto OPERATING_SYSTEM per verificare lo stato dei processi applicativi ed effettuare un loro eventuale restart").
soluzione(sovraccarico,serv,db,"L'applicazione sta presentando difficoltà nell'elaborazione della richiesta a causa di un carico di lavoro eccessivo da parte del database. Si consiglia di aprire una segnalazione al supporto DATABASE per verificare la disponibilità di risorse sul db").
soluzione(sovraccarico,serv,mail,"L'applicazione sta presentando difficoltà nell'elaborazione della richiesta a causa di un utilizzo intensivo del server di posta elettronica. Si consiglia di aprire una segnalazione al supporto APPLICATION per verificare la disponibilità di risorse sul server di posta dedicato").
soluzione(sovraccarico,network,traffico,"L'applicazione sta presentando difficoltà nell'elaborazione della richiesta a causa di utilizzo intensivo da parte delle risorse di rete. Si consiglia di aprire una segnalazione al supporto NETWORK per verificare il volume del traffico attualmente presente sulla LAN").

soluzione(network,temporaneo,refresh,"Si è verificata una perdita di connettività di rete, si consiglia di ripetere l'azione. Se il problema persiste contattare il supporto NETWORK").
soluzione(network,temporaneo,trasferimento,"Il trasferimento dei dati procede con estrema lentezza. La causa può essere dovuta ad un traffico di rete elevato dovuto ai molti utenti connessi").

