detail :- write("Il sistema OSES nasce come soluzione di supporto nella gestione e risoluzione di malfunzionamenti"),nl, 
          write("in ambito IT applications. E'possibile individuare le seguenti classi di anomalie: "),nl,
          nl,
          write("- Errori accidentali"), nl,
          write("- Anomalie di sicurezza"), nl,
          write("- Anomalie software"), nl,
          write("- Anomalie hardware"),nl,
          write("- Anomalie prestazionali"), nl,
          write("- Anomalie di rete"), nl,
          write("- Indisponibilità sistemi"), nl.
          
