% Module: trasforma.pl
% --------
%
% Trasformazione fatti asseriti in frasi da mostrare all'utente
% 

:- module(trasforma,
                [trasforma_incident/3,
                 trasforma_detail/3,
                 trasforma_criticita/2,
                 trasforma_perche/3,
                 trasforma_fatti/2,
                 trasforma_come/2,
                 trasforma_risp_incert/2,
                 trasforma_domanda/2,
                 trasforma_risp/3]).

%:- use_module(incertezza,[cert/2]).

%assegnazione dei valori testuali in base agli incident
trasforma_incident(incident,err_generico,"Errore generico ").
trasforma_incident(incident,security,"Security").
trasforma_incident(incident,sw,"Anomalia software ").
trasforma_incident(incident,network,"Network").
trasforma_incident(incident,sovraccarico,"Problemi prestazionali ").
trasforma_incident(incident,hw,"Anomalia hardware ").
trasforma_incident(incident,crash,"Indisponibilità sistemi").


%assegnazione dei valori testuali in base ai dettagli degli incident
trasforma_detail(dettaglio,dispositivo,"Strumenti di accesso personale").
trasforma_detail(dettaglio,user,"User/Account").
trasforma_detail(dettaglio,firewall,"Restrizioni di accesso/utilizzo").
trasforma_detail(dettaglio,malware,"Presenza di prodotti malevoli").
trasforma_detail(dettaglio,client,"Client").
trasforma_detail(dettaglio,serv,"Server").
trasforma_detail(dettaglio,db,"Anomalia su database applicativo").
trasforma_detail(dettaglio,disk,"Disk failure").
trasforma_detail(dettaglio,cpuram,"CPU/RAM failure").
trasforma_detail(dettaglio,temporaneo,"Problema temporaneo di connettività").
trasforma_detail(dettaglio,network,"Network failure").
trasforma_detail(dettaglio,dispositivo,"Dispositivo di accesso personale").

%assegnazione dei valori testuali in base ai problemi rilevati
trasforma_problem_letter(frequenza_utilizzo,alta,"il sistema viene utilizzato molto frequentemente").
trasforma_problem_letter(frequenza_utilizzo,bassa,"il sistema viene utilizzato poco frequentemente").
trasforma_problem_letter(frequenza_utilizzo,nonso,"frequenza di utilizzo del sistema non definita").

trasforma_problem_letter(finalita,query,"l'utente sta effettuando interrogazioni su una banca dati").
trasforma_problem_letter(finalita,report,"l'utente sta utilizzando un sistema per la gestione di documenti").
trasforma_problem_letter(finalita,mail,"l'utente sta utilizzando la posta elettronica").
trasforma_problem_letter(finalita,nonso,"finalità di utilizzo del sistema non definite").
trasforma_problem_letter(finalita,altro,"l'utente riscontra problemi durante l'utilizzo di funzionalità di sistema").

trasforma_problem_letter(durata_nel_tempo,prolungata,"il problema si verifica in modo continuativo nel tempo").
trasforma_problem_letter(durata_nel_tempo,intermittente,"il problema è di natura temporanea").

trasforma_problem_number(minuti,DOWN,"Il problema persiste da meno di 5 minuti") :- DOWN >= 0, DOWN < 5, !.
trasforma_problem_number(minuti,DOWN,"Il problema persiste da oltre 10 minuti") :- DOWN >= 10, DOWN < 20, !.
trasforma_problem_number(minuti,DOWN,"Il problema persiste da oltre 20 minuti") :- DOWN >= 20, DOWN < 60, !.
trasforma_problem_number(minuti,DOWN,"Il problema persiste da oltre 60 minuti") :- DOWN >= 60.

trasforma_problem_number(frequenza_problemi_analoghi,FR,"in passato hai rilevato problemi analoghi più di 5 volte") :- FR >= 5, !.
trasforma_problem_number(frequenza_problemi_analoghi,FR,"in passato hai rilevato problemi analoghi meno di 5 volte") :- FR >= 0, FR < 5.

trasforma_problem_number(mancata_autenticazione,MA,"il problema si verifica in fase di autenticazione") :- MA > 0.5, MA =< 1, !.
trasforma_problem_number(mancata_autenticazione,MA,"l\'autenticazione al sistema va a buon fine") :- MA >= 0, MA < 0.5.

trasforma_problem_number(problemi_analoghi,PA,"il problema si è manifestato altre volte in maniera analoga") :- PA > 0.5, PA =< 1, !.
trasforma_problem_number(problemi_analoghi,PA,"il problema non si è mai verificato in passato") :- PA >= 0, PA < 0.5.

trasforma_problem_number(piu_utenti_coinvolti,USERS,"il problema è esteso a più utenti") :- USERS > 0.5, USERS =< 1, !.
trasforma_problem_number(piu_utenti_coinvolti,USERS,"il problema è circoscritto solo alla tua utenza") :- USERS >= 0, USERS < 0.5.

trasforma_problem_number(piu_applicazioni_coinvolte,APP,"il problema è rilevato su altri sistemi") :- APP > 0.5, APP =< 1, !.
trasforma_problem_number(piu_applicazioni_coinvolte,APP,"il problema è rilevato solo su un singolo sistema") :- APP >= 0, APP < 0.5.

trasforma_problem_number(registrazione_al_sistema,REG,"l\' utente è già registrato al sistema") :- REG > 0.5, REG =< 1, !.
trasforma_problem_number(registrazione_al_sistema,REG,"l\' utente non è ancora registrato al sistema") :- REG >= 0, REG < 0.5.

trasforma_problem_number(rimane_su_pagina_iniziale,RED,"l\'utente visualizza nuovamente la stessa pagina dopo l\'immissione delle credenziali") :- RED > 0.5, RED =< 1, !.
trasforma_problem_number(rimane_su_pagina_iniziale,RED,"le credenziali inserite sono corrette") :- RED >= 0, RED < 0.5.

trasforma_problem_number(tentativi_login,T,"non è stato effettuato alcun tentativo di inserimento password ") :- T == 0, !.
trasforma_problem_number(tentativi_login,T,"è stato effettuato un numero di tentativi di inserimento password inferiore a 4") :- T > 0, T < 4, !.
trasforma_problem_number(tentativi_login,T,"è stato effettuato un numero di tentativi di inserimento passwordsuperiore a 4") :- T >= 4.

trasforma_problem_number(disconnessione_login,D,"user/pwd sono corrette, ma si riscontrano errori durante il tentativo di login") :- D > 0.5, D =< 1, !.
trasforma_problem_number(disconnessione_login,D,"non si riscontrano errori durante il tentativo di login") :- D >= 0, D < 0.5, !.

trasforma_problem_number(disconnessione_utilizzo,D,"si rilevano disconnessioni impreviste durante l'utilizzo") :- D > 0.5, D =< 1, !.
trasforma_problem_number(disconnessione_utilizzo,D,"non si rilevano disconnessioni impreviste durante l'utilizzo") :- D >= 0, D < 0.5, !.

trasforma_problem_number(connettivita,B,"la finestra di accesso iniziale non viene mostrata") :- B > 0.5, B =< 1, !.
trasforma_problem_number(connettivita,B,"la finestra di accesso iniziale viene correttamente mostrata") :- B >= 0, B < 0.5.

trasforma_problem_number(err_code,E,"la richiesta presenta una sintassi errata") :- E == 400, !.
trasforma_problem_number(err_code,E,"non si dispone dei permessi di accesso alla risorsa") :- E == 401, !.
trasforma_problem_number(err_code,E,"non si dispone dei permessi di accesso alla risorsa") :- E == 403, !.
trasforma_problem_number(err_code,E,"la risorsa richiesta non è disponibile") :- E == 404, !.
trasforma_problem_number(err_code,E,"si sono verificati errori di elaborazione delle richieste da parte del server") :- E == 500, !.
trasforma_problem_number(err_code,E,"si sono verificati problemi di rete") :- E == 502, !.
trasforma_problem_number(err_code,E,"il server non è al momento disponibile") :- E == 503.

trasforma_problem_number(dispositivi,DIS,"si rilevano problemi con l'utilizzo di strumenti di accesso personali ") :- DIS > 0.5, DIS =< 1, !.
trasforma_problem_number(dispositivi,DIS,"non si rilevano problemi con l'utilizzo di strumenti di accesso personali") :- DIS >= 0, DIS < 0.5.

trasforma_problem_number(non_affidabile,NA,"sul tuo PC è attivo un programma antivirus") :- NA > 0.5, NA =< 1, !.
trasforma_problem_number(non_affidabile,NA,"sul tuo sistema non è presente nessun programma antivirus") :- NA >= 0, NA < 0.5.

trasforma_problem_number(malware,NA,"il browser presenta a video informazioni indesiderate") :- NA > 0.5, NA =< 1, !.
trasforma_problem_number(malware,NA,"le informazioni richeiste sono correttamente visualizzate") :- NA >= 0, NA < 0.5.

trasforma_problem_number(browser,B,"il problema è rilevato utilizzando più browser") :- B > 0.5, B =< 1, !.
trasforma_problem_number(browser,B,"il problema è relativo ad un solo browser") :- B >= 0, B < 0.5.

trasforma_problem_number(funzioni,B,"l'applicazione carica tutte le funzioni a disposizione") :- B > 0.5, B =< 1, !.
trasforma_problem_number(funzioni,B,"l'applicazione non carica alcune funzioni a disposizione") :- B >= 0, B < 0.5.

trasforma_problem_number(info_scorrette,B,"la ricerca sul db produce risultati diversi da quelli richiesti") :- B > 0.5, B =< 1, !.
trasforma_problem_number(info_scorrette,B,"la ricerca sul db produce i risultati richiesti") :- B >= 0, B < 0.5.

trasforma_problem_number(info_mancanti,B,"la ricerca sul db non produce risultati") :- B > 0.5, B =< 1, !.
trasforma_problem_number(info_mancanti,B,"la ricerca sul db produce i risultati richiesti") :- B >= 0, B < 0.5.

trasforma_problem_number(posta,B,"riscontrato mancato invio/ricezione di email") :- B > 0.5, B =< 1, !.
trasforma_problem_number(posta,B,"l'invio/ricezione di email funziona correttamente") :- B >= 0, B < 0.5.

trasforma_problem_number(allegati,B,"riscontrati problemi con invio allegati") :- B > 0.5, B =< 1, !.
trasforma_problem_number(allegati,B,"nessun problema riscontrato con invio allegati") :- B >= 0, B < 0.5.

trasforma_problem_number(visibilita,B,"riscontrata mancanza di alcuni file all'interno delle rispettive directory") :- B > 0.5, B =< 1, !.
trasforma_problem_number(visibilita,B,"tutti i file sono presenti all'interno delle rispettive directory") :- B >= 0, B < 0.5.

trasforma_problem_number(doc,B,"riscontrati errori nell'effettuare operazioni di apertura/modifica/salvataggio sui documenti") :- B >= 0, B < 0.5, !.
trasforma_problem_number(doc,B,"si riesce ad effettuare le normali operazioni sui documenti") :- B > 0.5, B =< 1.

trasforma_problem_number(transfer,B,"riscontrati errori in fase di upload/download dei documenti") :- B > 0.5, B =< 1, !.
trasforma_problem_number(transfer,B,"non riscontrati errori in fase di upload/download dei documenti") :- B >= 0, B < 0.5.

trasforma_problem_number(schedulazioni,B,"riscontrati errori nell'esecuzione di procedure schedulate") :- B > 0.5, B =< 1, !.
trasforma_problem_number(schedulazioni,B,"nessun errore nell'esecuzione di procedure schedulate") :- B >= 0, B < 0.5.

trasforma_problem_number(stampa,B,"rilevati problemi in fase di stampa") :- B > 0.5, B =< 1, !.
trasforma_problem_number(stampa,B,"nessun problema rilevato in fase di stampa") :- B >= 0, B < 0.5.

trasforma_problem_number(orario,B,"applicazione sincronizzata correttamente col proprio sistema") :- B > 0.5, B =< 1, !.
trasforma_problem_number(orario,B,"applicazione non sincronizzata correttamente col proprio sistema") :- B >= 0, B < 0.5.

trasforma_problem_number(lentezza_server,L,"sono stati rilevati problemi di lentezza sul sistema") :- L > 0.5, L =< 1, !.
trasforma_problem_number(lentezza_server,L,"non sono stati rilevati problemi di lentezza sul sistema") :- L >= 0, L < 0.5.

trasforma_problem_number(lentezza_pc,L,"sono stati rilevati problemi di lentezza sul proprio pc") :- L > 0.5, L =< 1, !.
trasforma_problem_number(lentezza_pc,L,"non sono stati rilevati problemi di lentezza sul proprio pc") :- L >= 0, L < 0.5.

trasforma_problem_number(lentezza_rete,L,"sono stati rilevati problemi di lentezza relativi alla rete") :- L > 0.5, L =< 1, !.
trasforma_problem_number(lentezza_rete,L,"non sono stati rilevati problemi di lentezza relativi alla rete") :- L < 0.5.

trasforma_problem_number(blocco,BL,"rilevato blocco del sistema") :- BL > 0.5, BL =< 1, !.
trasforma_problem_number(blocco,BL,"non rilevato blocco del sistema") :- BL >= 0, BL < 0.5.


%assegnazione delle soglie in base alla criticità rilevata
trasforma_criticita(S,Severity) :- S >= 0, S =< 1, Severity = 'bassa', !.
trasforma_criticita(S,Severity) :- S > 1, S < 2.5, Severity = 'medio-bassa', !.
trasforma_criticita(S,Severity) :- S >= 2.5, S < 3, Severity = 'medio-alta', !.
trasforma_criticita(S,Severity) :- S >= 3, Severity = 'alta'. 

%assegnazione dei valori testuali in base alle risposte incerte
trasforma_risp_incert(VAL,Risp) :- VAL == 1, Risp = 'si', !.
trasforma_risp_incert(VAL,Risp) :- VAL == 0.6, Risp = 'forse si', !.
trasforma_risp_incert(VAL,Risp) :- VAL == 0.5, Risp = 'nonso', !.
trasforma_risp_incert(VAL,Risp) :- VAL == 0.2, Risp = 'forse no', !. 
trasforma_risp_incert(VAL,Risp) :- VAL == 0, Risp = 'no'. 

%assegnazione dei valori alle risposte in base al tipo di domanda sottoposta
trasforma_risp(Val,X,frequenza) :- X == 1, Val = 'alta', !.
trasforma_risp(Val,X,frequenza) :- X == 2, Val = 'bassa', !.
trasforma_risp(Val,X,frequenza) :- X == 3, Val = 'nonso'.
 
trasforma_risp(Val,X,finalita) :- X == 1, Val = 'query', !.
trasforma_risp(Val,X,finalita) :- X == 2, Val = 'report', !.
trasforma_risp(Val,X,finalita) :- X == 3, Val = 'mail', !.
trasforma_risp(Val,X,finalita) :- X == 4, Val = 'altro', !.
trasforma_risp(Val,X,finalita) :- X == 5, Val = 'nonso'.

trasforma_risp(Val,X,ricorrenza) :- X == 1, Val = 'intermittente', !.
trasforma_risp(Val,X,ricorrenza) :- X == 2, Val = 'prolungata', !.
trasforma_risp(Val,X,ricorrenza) :- X == 3, Val = 'nonso'.


trasforma_come(VAL,_) :- \+(number(VAL)),
                           write(', a cui hai risposto \''),
                           write(VAL),write('\''), !.
trasforma_come(VAL,TYPE) :- number(VAL), 
                                   TYPE == incert,
                                   trasforma_risp_incert(VAL,Risp),
                                   write(' a cui hai risposto \''),
                                   write(Risp),write('\''), !.
trasforma_come(VAL,TYPE) :- number(VAL), 
                                   TYPE == numeric,
                                   write(' a cui hai risposto \''),
                                   write(VAL),write('\'').


trasforma_perche(Problem,VAL,Text) :- not(number(VAL)),
                                       trasforma_problem_letter(Problem,VAL,Text), !.
trasforma_perche(Problem,VAL,Text) :- number(VAL),
                                       trasforma_problem_number(Problem,VAL,Text).


trasforma_fatti(VAL,_) :- \+(number(VAL)),
                           write('con valore '),
                           write('\''),
                           write(VAL),write('\''), !.
trasforma_fatti(VAL,T) :- number(VAL), 
                           T == incert,
                           write('con valore '),
                           trasforma_risp_incert(VAL,Risp),
                           write('\''),
                           write(Risp),write('\''), !.
trasforma_fatti(VAL,T) :- number(VAL), 
                           T == numeric,
                           write('con valore '),
                           write('\''),
                           write(VAL),write('\'').
                           

trasforma_domanda(VAL,_) :- \+(number(VAL)),
                           write(VAL), !.
trasforma_domanda(VAL,T) :- number(VAL), 
                           T == incert,
                           trasforma_risp_incert(VAL,Risp),
                           write(Risp), !.
trasforma_domanda(VAL,T) :- number(VAL), 
                            T == numeric,
                            write(VAL).

