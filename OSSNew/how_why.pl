% Module: how_why.pl
% --------
%
% Il modulo contiene le spiegazioni a tutte le domande 
%che serviranno a fornire spiegazioni
% 

:- module(how_why,
                [spiega_come/0,
                 spiega_perche/1]).

:- use_module(library(lists)).
:- use_module(trasforma,
                [trasforma_come/2,
                 trasforma_perche/3]).
:- use_module(utility,
                 [first/2]).

%Spiegazioni di carattere generale
spiega_domanda(frequenza_utilizzo,'un utilizzo frequente del sistema implica una maggiore conoscenza delle sue funzionalità applicative').
spiega_domanda(durata_nel_tempo,'il problema riscontrato potrebbe presentarsi in modo temporaneo (può anche rientrare autonomamente) o persistere nel corso del tempo (è necessario un intervento esterno)').
spiega_domanda(minuti,'un tempo di inattività elevato (es. oltre i 15 minuti) implica un problema più critico').
spiega_domanda(piu_applicazioni_coinvolte,'l\'impatto complessivo del problema può essere maggiore se diffuso su scala più ampia').
spiega_domanda(piu_utenti_coinvolti,'l\'impatto complessivo del problema potrebbe essere minore se circoscritto alla singola utenza che utilizza il sistema').
spiega_domanda(problemi_analoghi,'un problema rilevato più volte in passato in maniera analoga indica anomalie già presenti sul sistema, ma non ancora riscontrate/risolte').
spiega_domanda(frequenza_problemi_analoghi,'un numero maggiore di occorrenze del problema è sinonimo di anomalie critiche già presenti sul sistema, ma non ancora riscontrate/risolte').

%Spiegazioni relative all' autenticazione
spiega_domanda(mancata_autenticazione,'la mancata autenticazione al sistema non permette l\'utilizzo di tutte le funzionalità a disposizione').
spiega_domanda(registrazione_al_sistema,'il primo accesso al sistema consente di ricevere le credenziali personali necessarie all\'utilizzo delle funzionalità').
spiega_domanda(rimane_su_pagina_iniziale,'dopo aver inserito user/pwd ed aver inviato la richiesta, il sistema la elabora ma viene ripresentato nuovamente il form iniziale').
spiega_domanda(tentativi_login,'un numero di tentativi di login errati abbastanza alto può essere riconducibile ad anomalie collegate alle credenziali in tuo possesso ').
spiega_domanda(disconnessione_login,'le credenziali sono esatte, tuttavia un errore di questo genere può indicare che all\'utenza non sono associati tutti i permessi di accesso necessari').
spiega_domanda(dispositivi,'gli strumenti personali consentono l\'accesso sicuro e certificato a tutte le funzionalità applicative').
spiega_domanda(connettivita,"la mancata visualizzazione della pagina iniziale non consente l'inserimento delle credenziali di accesso, e può essere sintomo di problemi di connettività").
spiega_domanda(non_affidabile,'quando si accede a siti web sicuri vengono utilizzati certificati digitali in modo che le informazioni siano inviate solamente al destinatario e non siano intercettate da terzi.').
spiega_domanda(malware,'la visualizzazione di informazioni potenzialmente dannose potrebbe indicare presenza di prodotti software indesiderati auto-installati sul proprio pc').
spiega_domanda(browser,'il software di navigazione che si sta impiegando potrebbe contenere al suo interno componenti (estensioni) indesiderate auto-installate').
spiega_domanda(err_code,'I codici 4xx indicano generalmente errori riconducibili ad anomalie client, mentre errori con codice 5xx sono generalmente associati ad anomalie lato server').
spiega_domanda(lentezza_pc,'la lentezza può essere causata da utilizzo intensivo da parte delle risorse del proprio computer').
spiega_domanda(lentezza_server,'la lentezza può essere causata da utilizzo intensivo da parte del server applicativo').
spiega_domanda(lentezza_rete,'la lentezza può essere causata da presenza di traffico elevato sulla rete').
spiega_domanda(blocco,'l\'applicazione risulta disponibile, ma non si riesce ad eseguire i comandi fondamentali. Ciò può essere sintomo di blocco completo del sistema').


%%Domande relative alle funzionalità
spiega_domanda(finalita,'le finalità dell\'utente sono strettamente relazionate alle funzionalità che si intende utilizzare').

spiega_domanda(funzioni,'la mancata presenza di alcune funzionalità all\'interno dell\'applicazione può essere sintomo di errori di caricamento da parte del server').
spiega_domanda(disconnessione_utilizzo,'la disconnessione improvvisa durante l\'esecuzione dei comandi può essere sintomo di problemi di connettività').
spiega_domanda(info_scorrette,'una ricerca sul db talvolta può fornire in output risultati che non rispecchiano completamente i dati richiesti dall\'utente').
spiega_domanda(info_mancanti,'una ricerca sul db che produce risultati incompleti può essere sintomo di errori di elaborazione da parte della base dati').
spiega_domanda(visibilita,'la mancata visibilità di alcuni files può essere sintomo di presenza di guasti sui supporti di memorizzazione').
spiega_domanda(doc,'le operazioni consentite sui documenti riguardano apertura-modifica-salvataggio').
spiega_domanda(transfer,'i problemi di trasferimento documenti riguardano il download-upload da e verso il sistema che stai utilizzando. ').
spiega_domanda(posta,'il problema potrebbe essere relativo ai server di posta (nel caso in cui coinvolga più utenti), mentre potrebbe essere necessario controllare le impostazioni locali qualora il problema riguardasse solo la specifica utenza').
spiega_domanda(allegati,'l\'integrazione di allegati all\'interno dell\'email completa il messaggio di posta con informazioni aggiuntive').
spiega_domanda(stampa,'le stampanti possono essere utilizzate da più utenti contemporaneamente: questo può creare code nella gestione delle operazioni di stampa').
spiega_domanda(schedulazioni,'le procedure schedulate consentono l\'esecuzione automatica di task preimpostati, come ad esempio la generazione giornaliera di nuovi report').
spiega_domanda(orario,'un orario non allineato tra PC e applicazione può portare ad errori in fase di elabroazione dati').


spiega_come :- attivate(X),nl,
               write('Spiegazione: '),nl,
               write('Regole utilizzate: '),
               reverse(X,Invert),
               sort(Invert,Inverted),
               stampa_regole(Inverted),nl,
               last(Inverted,Last),
               findall(EXPLAIN,(rule Last:_ ==> _ explained_by EXPLAIN severity _),ListExplain),
               flatten(ListExplain,L),
               write('Come sono arrivato a queste soluzioni: '),nl,
               findall(ID-IDRegola-Problem-VAL-TYPE,
                           (member(IDRegola,L),
                            rule IDRegola:LHS ==> _ explained_by _ severity _,
                            member(risposta(ID,TYPE,problem(Problem,VAL)),LHS),
                            fact(risposta(ID,TYPE,problem(Problem,VAL)))),
                         ListaCome),
               sort(ListaCome,LC),
               stampa_come(LC).

stampa_regole([]).
stampa_regole([Head|Tail]) :- write(Head),write('  '),
                              stampa_regole(Tail).

stampa_come([]).
stampa_come([ID-IDRegola-Problem-VAL-T|Tail]) :- write(' - Dalla domanda '),
                                      write(ID),write(') relativa al problema \''),
                                      write(Problem),write('\''),
                                      trasforma_come(VAL,T),
                                      write(' ho riscontrato quanto segue: '),
                                      trasforma_perche(Problem,VAL,Text),write(Text),
                                      write(' (attraverso la regola '),
                                      write(IDRegola),write(')'),nl,
                                      stampa_come(Tail).

spiega_perche(Domanda) :- 
               daattivare(Y),
               write('Ti ho posto questa domanda perchè: '), nl, write("- "),
               spiega_domanda(Domanda,Spiegazione),
               write(Spiegazione),nl,
               first(Y,First),
               findall(EXPLAIN,(rule First:_ ==> _ explained_by EXPLAIN severity _),ListExplain),
               flatten(ListExplain,L),
               findall(Problem-VAL,
                          (member(IDRule,L),
                           rule IDRule:LHS ==> _ explained_by _ severity _,
                           member(risposta(ID,_,problem(Problem,VAL)),LHS),
                           fact(risposta(ID,_,problem(Problem,VAL)))),
                         ListaProblem),
              stampa_perche(ListaProblem).


stampa_perche([]).
stampa_perche([Problem-VAL|Tail]) :- 
                                      write("- "),
                                      trasforma_perche(Problem,VAL,Text),
                                      write(Text),nl,
                                      stampa_perche(Tail).


