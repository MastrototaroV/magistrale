% Module: modifica_azioni.pl
% --------
%
% Il modulo contiene i predicati necessari al salvataggio delle azioni (per ogni regola) e alla ripartenza da un punto specifico del sistema
%
%

:- module(modifica_azioni,
             [ritratta/1,
              salva/2,
              disfai/1,
              recoverState/1,
              ripristinaID/1,
              riparti/0]).

:- use_module(library(lists)).
:- use_module(trasforma,
                [trasforma_domanda/2]).
:- use_module(utility,
                [estraiLista/3,
                 diff/3]).

:- dynamic azioni/3.

ritratta([]) :- !.
ritratta([ID|Rest]) :-
    disfai(ID),
    ritratta(Rest).


recoverState([]) :- !.
recoverState([ID|Rest]) :-
    ripristinaID(ID),
    recoverState(Rest).



salva(ID,assert(X)) :-
    retract(azioni(ID,Rest,Old)),
    assert(azioni(ID,['retract'|Rest],[X|Old])),
    !.


salva(ID,assert(X)) :-
    assert(azioni(ID,['retract'],[X])),
    !.




salva(ID,asserta(X)) :-
    retract(azioni(ID,Rest,Old)),
    assert(azioni(ID,['retract'|Rest],[X|Old])),
    !.


salva(ID,asserta(X)) :-
    assert(azioni(ID,['retract'],[X])),
    !.




salva(ID,assertz(X)) :-
    retract(azioni(ID,Rest,Old)),
    assert(azioni(ID,['retract'|Rest],[X|Old])),
    !.

salva(ID,assertz(X)) :-
    assert(azioni(ID,['retract'],[X])),
    !.



salva(ID,retract(X)) :-
    retract(azioni(ID,Rest,Old)),
    assert(azioni(ID,['assert'|Rest],[X|Old])),
    !.


salva(ID,retract(X)) :-
    assert(azioni(ID,['assert'],[X])),
    !.


salva(_,_) :- !.


riparti :- 
	 domande(Question),
     attivate(Attivate),
     daattivare(DaAttivare),
     retract(attivate(Attivate)),
     retract(daattivare(DaAttivare)),
     retract(domande(Question)),
     write('Inserire domanda da riformulare: '),
     read(ID),
     member(ID,Question),
     extractID(Attivate,ID,IDInvert),
     nth0(IDInvert,Attivate,IDRule),
     estraiLista(Question,ID,NewQuestion),
     estraiLista(Attivate,IDRule,NewAttivate),nl,
     recoverState(NewAttivate),
     diff(Question,NewQuestion,UpdatedQuestion),
     diff(Attivate,NewAttivate,UpdatedAttivate),
     assert(domande(UpdatedQuestion)),
     assert(daattivare([])),
     assert(attivate(UpdatedAttivate)).
     


extractID(Attivate,ID,IDInvert) :- length(Attivate,L),
                                   IDInvert is L - ID.
         
         
disfai(ID) :-
    retract(azioni(ID,_,_)).
  
  
ripristinaID(ID) :-
	retract(azioni(ID,ACT,Facts)),
    ripristina(ACT,Facts).
    

ripristina([],[]) :- !.    
ripristina([A|Resta],[F|RestF]) :- A == 'assert',
                           take(assert(F)),
                           ripristina(Resta,RestF).
ripristina([A|Resta],[F|RestF]) :- A == 'retract',
                           take(retract(F)),
                           ripristina(Resta,RestF).