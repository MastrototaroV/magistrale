:- op(810, fx, rule).
:- op(500, xfy, :).
:- op(800, xfx, ==>).
:- op(700, xfy, severity).
:- op(600, xfy, explained_by).

:- ensure_loaded(soluzioni).
:- ensure_loaded(details).
:- ensure_loaded(help).  
:- ensure_loaded(modifica_azioni). 

:- use_module(interface_shell, 
                 [intro/0,
                  domanda/4,
                  mostra_soluzioni/0,
                  riesegui/0,
                  riesegui_restart/0,
                  menu_init/0,
                  menu/0]).
:- use_module(incertezza,
                 [calcola_certezza/3,
                  cert_risp_neg/2]).
:- use_module(utility,
                 [question_id/1,
                  first/2,
                  sort_list/2,
                  extract_id/2]).

:- dynamic attivate/1.
:- dynamic daattivare/1.
:- dynamic domande/1.

attivate([]).
daattivare([]).
domande([]).

run :- intro, 
       menu_init.

% carica la base di conoscenza
eseguiload :-
    write('Inserisci il nome del file da caricare (tra apici): '),
    read(FILE),
	consult(FILE),nl,
	write('KB caricata correttamente'), nl.  

start :-  reload,
          init,
          eseguiavvio.     

% inzializza la working memory con la lista dei valori dichiarati nella KB
init :- asserzioni(X),
        inizializza_lista(X). 

inizializza_lista([]) :- !.
inizializza_lista([Head|Tail]) :- assertz(fact(Head)),
	                              !,
	                              inizializza_lista(Tail).
 
eseguiavvio :- fact(goal(solution)), !,
               mostra_soluzioni,
               riesegui. 

eseguiavvio :- cs,
	           primaRegola(ID),
	           call(rule ID:LHS ==> RHS explained_by _ severity _ ),
               esegui_regola(ID,LHS,RHS),
               nextRegola(ID),
               !,
               eseguiavvio.

eseguiavvio :- nl, write('Sono arrivato alla seguente soluzione: '),
               nl, write('Nessuna anomalia riscontrata'), 
               riesegui_restart, !.

cs :- 
   attivate(X),
%  nl,write("attivate:"),write(X),
   daattivare(Y),
%   nl,write("da attivare:"),write(Y),
   findall(Num-ID,
            (call(rule ID:LHS ==> _ explained_by _ severity _ ),
             not(member(ID,X)),
             match(LHS),
             length(LHS,Num)),
             CS),
% nl,write("CS:"),write(CS),nl,
   sort_list(CS, CSOrd),
%  write("CS ORDINATO:"),write(CSOrd),nl,
   retract(daattivare(Y)),
   extract_id(CSOrd,New),
   assert(daattivare(New)).
%  write("NEW:"),write(New),nl.

match([]) :- !.
match([Prem|Rest]) :- (fact(Prem);test(Prem)),
                       match(Rest), 
                       !.

primaRegola(ID) :- daattivare(Y),
                   first(Y,ID).

esegui_regola(ID,LHS,RHS) :- match(LHS),
	                         process(ID,RHS).

process(_,[]) :- !.
process(ID,[Head|Tail]) :- take(Head),
                           salva(ID,Head),
	                       process(ID,Tail).


nextRegola(ID) :- 
	retract(daattivare([ID|L])),
	assert(daattivare(L)),
    retract(attivate(List)),
	assert(attivate([ID|List])). 


%chiude l'esecuzione azzerando la lista delle regole e dei fatti asseriti e la lista delle domande sottoposte
reload :- attivate(X),
          retract(attivate(_)),
          retract(daattivare(_)),
          retract(domande(_)),
          assert(attivate([])),
          assert(daattivare([])),
          assert(domande([])),
          ritratta(X),
          retractall(fact(_)).

% test consentiti nella LHS
test(X == Y) :- X == Y, !.
test(X > Y) :- X > Y, !.
test(X >= Y) :- X >= Y, !.
test(X < Y) :- X < Y, !.
test(X =< Y) :- X =< Y, !.
test(X = Y) :- X is Y, !.
test(member(X,Y)) :- member(X,Y).

%azioni consentite nella RHS
take(retract(X)) :- retract(fact(X)), !.
take(assert(X)) :- asserta(fact(X)), !.
take(X == Y) :- X == Y, !.
take(X < Y) :- X < Y, !.
take(X > Y) :- X > Y, !.
take(X = Y) :- X is Y, !.
take(X >= Y) :- X >= Y, !.
take(X =< Y) :- X =< Y, !.
take(write(X)) :- write(X), !.
take(nl) :- nl, !.
take(read(X)) :- read(X).

take(domanda(ID,Domanda,ValoreRisp,incert)) :- domanda(ID,Domanda,ValoreRisp,incert), !.
take(domanda(ID,Domanda,ValoreRisp,frequenza)) :- domanda(ID,Domanda,ValoreRisp,frequenza), !.
take(domanda(ID,Domanda,ValoreRisp,finalita)) :- domanda(ID,Domanda,ValoreRisp,finalita), !.
take(domanda(ID,Domanda,ValoreRisp,ricorrenza)) :- domanda(ID,Domanda,ValoreRisp,ricorrenza), !.
take(domanda(ID,Domanda,ValoreRisp,numeric)) :- domanda(ID,Domanda,ValoreRisp,numeric), !.

take(calcola_certezza(OLD, C, NEW)) :- calcola_certezza(OLD, C, NEW).

take(cert_risp_neg(OLD, NEW)) :- cert_risp_neg(OLD, NEW).

take(question_id(ID)) :- question_id(ID).




