% Module: incertezza.pl
% --------
%
% Il modulo contiene i predicati necessari alla gestione dell'incertezza
% 

:- module(incertezza,[associa_val_cert/3,
                      calcola_certezza/3,
                      cert_risp_neg/2,
                      approssima_cert/2]).
                      
:- use_module(utility,[avg/2]).

%assegnazione dei valori alle risposte in base al loro grado di certezza
associa_val_cert(Val,X,incert) :- X == 1, Val is 1, !.
associa_val_cert(Val,X,incert) :- X == 2, Val is 0.6, !.
associa_val_cert(Val,X,incert) :- X == 3, Val is 0.2, !. 
associa_val_cert(Val,X,incert) :- X == 4, Val is 0, !. 
associa_val_cert(Val,X,incert) :- X == 5, Val is 0.5.

%calcolo della certezza: vengono valutate le risposte contenute 
%nella parte antecedente della regola (media dei valori di certezza).
%successivamente è calcolata l'attendibilità della risposta sulla base del valore 
%precedente e di una soglia predefinita per ogni regola
calcola_certezza(ANT, C, CONS) :- calcola_certezza_antecedente(ANT, ANT_NEW),
                                  calcola_certezza_conseguente(ANT_NEW, C, CONS).
calcola_certezza_antecedente(ANT, ANT_NEW) :- avg(ANT, ANT_NEW).
calcola_certezza_conseguente(ANT, C, CONS) :- CONS is ANT * C.

%trasforma certezza in percentuale
approssima_cert(C,CERT) :- CERT is round(C*100).

%trasforma la probabilità della certezza in 1/cert
cert_risp_neg(CERT, CERT1) :- CERT1 is (1 - CERT).

