% Module: gui.pl
% --------
%
% Il modulo contiene i predicati necessari all'interazione con l'utente
%

:- module(interface_shell,
            [intro/0,
             domanda/4,
             mostra_soluzioni/0,
             lst/0,
             riesegui/0,
             riesegui_restart/0,
             menu_init/0,
             menu/0]).

:- use_module(trasforma,
                [trasforma_incident/3,
                 trasforma_detail/3,
                 trasforma_risp/3,
                 trasforma_perche/3,
                 trasforma_fatti/2,
                 trasforma_domanda/2,
                 trasforma_risp_incert/2]).
:- use_module(questions,
                [chiedi/2]).
:- use_module(incertezza,
                [associa_val_cert/3,
                 approssima_cert/2]).
:- use_module(how_why,
                [spiega_come/0,
                 spiega_perche/1]).
:- use_module(utility,
                [calcola_criticita/0,
                 first/2]).

% menu principale
intro:-
     write('**********************************************'),nl,
     write('* OSES (OPERATIONAL SUPPORT EXPERT SYSTEM) ***'),nl,
     write('**********************************************'),nl,
     write('* Benvenuto *'),nl,nl.

% azioni disponibili nel menu iniziale
menu_init :- 
     nl,write('COMANDI DISPONIBILI: '), nl,
     write('1. - carica'), nl,
     write('2. - dettagli'),nl,
     write('3. - help'),nl,
     write('4. - exit'),nl,
     write('>> '),
     read(MENUINIT),
     scelta_menu_init(MENUINIT).

scelta_menu_init(1) :- eseguiload, menu, !.
scelta_menu_init(2) :- detail, menu_init, !.
scelta_menu_init(3) :- comandi, menu_init, !.
scelta_menu_init(4) :- nl, write('Arrivederci ..'), nl, nl, halt, !.
scelta_menu_init(_) :- nl, write('Scelta non valida ...'), nl, menu_init.

% azioni disponibili nel menu principale
menu :- 
     nl,write('COMANDI DISPONIBILI: '), nl,
     write('1. - carica'), nl,
     write('2. - inizia analisi'),nl,
     write('3. - riepilogo'),nl,
     write('4. - dettagli'),nl,
     write('5. - help'),nl,
     write('6. - exit'),nl,
     write('>> '),
     read(MENU),
     scelta_menu(MENU).

scelta_menu(1) :- eseguiload, menu, !.
scelta_menu(2) :- start, !.
scelta_menu(3) :- lst, menu, !.
scelta_menu(4) :- detail, menu, !.
scelta_menu(5) :- comandi, menu, !.
scelta_menu(6) :- nl, write('Arrivederci ..'), nl, nl, halt, !.
scelta_menu(_) :- nl, write('Scelta non valida ...'), nl, menu.


 lst :-
 	write('Dalle analisi effettuate è stato rilevato quanto segue: '),nl,
     findall(P-V,
              (fact(problem(P,V))),
               ListaProblemi),
     write('Problem: '), nl,
 	stampa_problem(ListaProblemi).


stampa_problem([]) :- write(' nessun problem rilevato '), nl, !.
stampa_problem(L) :- stampa_problemi(L).

stampa_problemi([]).
stampa_problemi([P-V|Tail]) :- write('- '),trasforma_perche(P,V,Text),
                              write(Text),nl,
                              stampa_problemi(Tail).


% Menù domanda con risposta incerta
domanda(ID,Domanda,ValoreRisp,incert) :-
    nl,write(ID),write(')'),chiedi(Domanda,Text),write(Text),nl,
    write('Risposte disponibili: '),nl,
    write('1. - si '),nl,
    write('2. - forse si '),nl,
    write('3. - forse no '),nl,
    write('4. - no '),nl,
    write('5. - nonso '),nl,
    write('opt. - Per visualizzare le opzioni a disposizione '),nl,
    write('q. - Per interrompere l\'analisi e tornare al menù principale '),nl,
    write('>> '),
    read(INCERT),
    scelta_risp_incert(INCERT,ID,Domanda,ValoreRisp,incert).   
    
% Menù domanda con risposta "frequenza"
domanda(ID,Domanda,ValoreRisp,frequenza) :-    
    nl,write(ID),write(')'),chiedi(Domanda,Text),write(Text),nl,
    write('Risposte disponibili: '),nl,
    write('1. - alta. '),nl,
    write('2. - bassa. '),nl,
    write('3. - nonso. '),nl,
    write('opt. - Per visualizzare le opzioni a disposizione '),nl,
    write('q. - Per interrompere l\'analisi e tornare al menù principale '),nl,
    write('>> '),
    read(FREQ),
    scelta_risp_frequenza(FREQ,ID,Domanda,ValoreRisp,frequenza).    
    
% Menù domanda con risposta numerica
domanda(ID,Domanda,ValoreRisp,numeric) :-    
    nl,write(ID),write(')'),chiedi(Domanda,Text),write(Text),nl,
    write('opt. - Per visualizzare le opzioni a disposizione '),nl,
    write('q. - Per interrompere l\'analisi e tornare al menù principale '),nl,
    write('>> '),
    read(NUM),
    scelta_risp_numeric(NUM,ID,Domanda,ValoreRisp,numeric).  
    
% Menù domanda con risposta "finalita"
domanda(ID,Domanda,ValoreRisp,finalita) :-    
    nl,write(ID),write(')'),chiedi(Domanda,Text),write(Text),nl,
    write('Risposte disponibili: '),nl,
    write('1. - recupero informazioni. '),nl,
    write('2. - reportistica. '),nl,
    write('3. - messaggistica. '),nl,
    write('4. - altri servizi. '),nl,
    write('5. - nonso. '),nl,
    write('opt. - Per visualizzare le opzioni a disposizione '),nl,
    write('q. - Per interrompere l\'analisi e tornare al menù principale '),nl,
    write('>> '),
    read(FIN),
    scelta_risp_finalita(FIN,ID,Domanda,ValoreRisp,finalita).    

% Menù domanda con risposta "ricorrenza"
domanda(ID,Domanda,ValoreRisp,ricorrenza) :-    
    nl,write(ID),write(')'),chiedi(Domanda,Text),write(Text),nl,
    write('Risposte disponibili: '),nl,
    write('1. - intermittente. '),nl,
    write('2. - prolungata. '),nl,
    write('3. - nonso. '),nl,
    write('opt. - Per visualizzare le opzioni a disposizione '),nl,
    write('q. - Per interrompere l\'analisi e tornare al menù principale '),nl,
    write('>> '),
    read(RIC),
    scelta_risp_ricorrenza(RIC,ID,Domanda,ValoreRisp,ricorrenza). 
    
scelta_risp_incert(INCERT,_,_,ValoreRisp,incert) :- number(INCERT), INCERT >= 1, INCERT =< 5, associa_val_cert(ValoreRisp,INCERT,incert), !.
scelta_risp_incert(opt,ID,Domanda,ValoreRisp,incert) :- options(ID,Domanda,ValoreRisp,incert), !.
scelta_risp_incert(q,_,_,_,incert) :- nl, menu, !.
scelta_risp_incert(_,ID,Domanda,ValoreRisp,incert) :- nl, write('Scelta non valida ...'), nl, domanda(ID,Domanda,ValoreRisp,incert).

scelta_risp_frequenza(FREQ,_,_,ValoreRisp,frequenza) :- number(FREQ), FREQ >= 1, FREQ =< 3, trasforma_risp(ValoreRisp,FREQ,frequenza), !.
scelta_risp_frequenza(opt,ID,Domanda,ValoreRisp,frequenza) :- options(ID,Domanda,ValoreRisp,frequenza), !.
scelta_risp_frequenza(q,_,_,_,frequenza) :- nl, menu, !.
scelta_risp_frequenza(_,ID,Domanda,ValoreRisp,frequenza) :- nl, write('Scelta non valida ...'), nl, domanda(ID,Domanda,ValoreRisp,frequenza).

scelta_risp_numeric(NUM,_,_,ValoreRisp,numeric) :- number(NUM), NUM >=0, NUM =< 600, ValoreRisp = NUM, !.
scelta_risp_numeric(opt,ID,Domanda,ValoreRisp,numeric) :- options(ID,Domanda,ValoreRisp,numeric), !.
scelta_risp_numeric(q,_,_,_,numeric) :- nl, menu, !.
scelta_risp_numeric(_,ID,Domanda,ValoreRisp,numeric) :- nl, write('Scelta non valida ...'), nl, domanda(ID,Domanda,ValoreRisp,numeric).

scelta_risp_finalita(FIN,_,_,ValoreRisp,finalita) :- number(FIN), FIN >= 1, FIN =< 5, trasforma_risp(ValoreRisp,FIN,finalita), !.
scelta_risp_finalita(opt,ID,Domanda,ValoreRisp,finalita) :- options(ID,Domanda,ValoreRisp,finalita), !.
scelta_risp_finalita(q,_,_,_,finalita) :- nl, menu, !.
scelta_risp_finalita(_,ID,Domanda,ValoreRisp,finalita) :- nl, write('Scelta non valida ...'), nl, domanda(ID,Domanda,ValoreRisp,finalita), !.

scelta_risp_ricorrenza(RIC,_,_,ValoreRisp,ricorrenza) :- number(RIC), RIC >= 1, RIC =< 3, trasforma_risp(ValoreRisp,RIC,ricorrenza), !.
scelta_risp_ricorrenza(opt,ID,Domanda,ValoreRisp,ricorrenza) :- options(ID,Domanda,ValoreRisp,ricorrenza), !.
scelta_risp_ricorrenza(q,_,_,_,ricorrenza) :- nl, menu, !.
scelta_risp_ricorrenza(_,ID,Domanda,ValoreRisp,ricorrenza) :- nl, write('Scelta non valida ...'), nl, domanda(ID,Domanda,ValoreRisp,ricorrenza).
   
% Menù opzioni
options(ID,Domanda,ValoreRisp,TYPE) :- 
           nl,write('OPZIONI '),nl,
           write('- Puoi chiedere chiarimenti sulla domanda sottoposta digitando: '),nl,
           write('p. - perchè. '),nl,
           write('- Puoi mostrare le soluzioni parziali digitando: '),nl,
           write('f. - fatti. '),nl,
           write('- Puoi ripartire da una domanda già sottoposta digitando: '),nl,
           write('r. - riparti. '),nl,
           write('- Puoi tornare indietro e riprendere l\'esecuzione digitando: '),nl,
           write('d. - domanda. '),nl,
           write('>> '),
           read(OPT),
           scelta_risp_options(OPT,ID,Domanda,ValoreRisp,TYPE).
           
scelta_risp_options(p,ID,Domanda,ValoreRisp,TYPE) :- spiega_perche(Domanda), nl, !, domanda(ID,Domanda,ValoreRisp,TYPE).
scelta_risp_options(f,ID,Domanda,ValoreRisp,TYPE) :- lst, !, domanda(ID,Domanda,ValoreRisp,TYPE).
scelta_risp_options(d,ID,Domanda,ValoreRisp,TYPE) :- domanda(ID,Domanda,ValoreRisp,TYPE), !.          
scelta_risp_options(r,ID,Domanda,ValoreRisp,TYPE) :- domande(X), length(X,1), change_question, write('   nessuna domanda disponibile '), nl, !, domanda(ID,Domanda,ValoreRisp,TYPE). 
scelta_risp_options(r,_,_,_,_) :- change_question, riparti, !, eseguiavvio.
scelta_risp_options(_,ID,Domanda,ValoreRisp,TYPE) :- nl, write('Scelta non valida ...'), nl, options(ID,Domanda,ValoreRisp,TYPE).
           

%mostra tutte le soluzioni
mostra_soluzioni :-
     nl,write('Sono arrivato alla seguente soluzione: '),
     nl,
     write("- Incident: "),fact(incident(I)),trasforma_incident(incident,I,Incident),write(Incident),nl,
     write("- Dettaglio: "),fact(dettaglio(D)),trasforma_detail(dettaglio,D,Dettaglio),write(Dettaglio),nl,
     write("- Criticità: "),calcola_criticita,nl,
     write("- Con certezza: "),fact(certezza(U)),approssima_cert(U,Certezza),write(Certezza),write('%'),nl,
     write("- Azioni suggerite: "),fact(soluzione(Sol)),soluzione(I,D,Sol,Soluzioni),write(Soluzioni),nl.    

% Menù riesecuzione delle analisi
riesegui :- 
     nl,nl,
     write('OPZIONI '),nl,
     write('- Digita "c." se vuoi vedere come sono arrivato a tali conclusioni'), nl,
     write('- Digita "s." per effettuare una nuova analisi'), nl,
     write('- Digita "n." per tornare al menù principale'), nl,
     write('>> '),
     read(RIESEGUI),
     scelta_riesegui(RIESEGUI).
     
scelta_riesegui(c) :- spiega_come, riesegui_restart, !.
scelta_riesegui(s) :- start, !.
scelta_riesegui(n) :- nl, menu, !.
scelta_riesegui(_) :- write('Scelta non valida ...'), nl, riesegui.


% Menù riesecuzione delle analisi
riesegui_restart :- 
     nl,nl,
     write('OPZIONI '),nl,
     write('- Digita "s." per effettuare una nuova analisi'), nl,
     write('- Digita "n." per tornare al menù principale'), nl,
     write('>> '),
     read(RESTART), 
     scelta_riesegui_restart(RESTART).

scelta_riesegui_restart(s) :- start, !.
scelta_riesegui_restart(n) :- menu, !.
scelta_riesegui_restart(_) :- write('Scelta non valida ...'), nl, riesegui_restart.

change_question :- domande(X),
                   reverse(X,Invert),
                   write('Domanda    Problema    Risposta'), nl,
                   stampa_domande(Invert).

stampa_domande([]) :- write('   nessuna domanda disponibile '), nl, !.
stampa_domande(Invert) :- print_question(Invert).

print_question(Invert) :- 
	              member(ID,Invert),
                  fact(risposta(ID,T,problem(Problem,VAL))),
                  write(ID),write('       '),write(Problem),
                  write('        '),trasforma_domanda(VAL,T),nl,fail.  
print_question(_).



   
   