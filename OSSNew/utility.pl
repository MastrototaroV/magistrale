:- module(utility,[avg/2,
                   extract_id/2,
                   sort_list/2,
                   first/2,
                   add/3,
                   diff/3,
                   calcola_criticita/0,
                   estraiLista/3,
                   question_id/1]).

:- use_module(library(lists)).
:- use_module(trasforma,
                [trasforma_criticita/2]).


%predicati per il calcolo della media aritmeitca 
%delle priorità relative alle risposte
avg(LIST, AVG) :- sum(LIST, SUM), conteggio(LIST, COUNT), AVG is SUM/COUNT.

sum([], 0).
sum([Head|Tail],RESULT) :- sum(Tail,PROVVISORIO), RESULT is Head + PROVVISORIO.

conteggio([],0).
conteggio([_|Tail], COUNT) :- conteggio(Tail, PROVVISORIO), COUNT is 1 + PROVVISORIO.



%predicato che restituisce l'ID relativo al numero di domanda da sottoporre all'utente 
question_id(ID) :- domande(ListaDomande),  
                   length(ListaDomande, L),
                   ID is L + 1,
                   retract(domande(ListaDomande)),
                   assertz(domande([ID|ListaDomande])).

%predicato che restituisce la criticità del problema
calcola_criticita :- attivate(X),
             first(X,IDRule),
             findall(EXPLAIN,(rule IDRule:_ ==> _ explained_by EXPLAIN severity _),ListExplain),
             flatten(ListExplain,L),
             findall(S,
                    (member(IDRule,L),
                     rule IDRule:_ ==> _ explained_by _ severity S),
                     ListSev),
             avg(ListSev,RESULT),
             trasforma_criticita(RESULT,Severity),
             write(Severity).   


%estrae l'ID della regola dalla lista contenente l'ID e la lunghezza della LHS
extract_id(CS,CSNew) :- 
estrai(CS,CSNew).

estrai([],[]) :- !.
estrai([_-X],[X]) :- !.
estrai([_-H|T],[H|NewT]):- estrai(T,NewT).


%ordina lista in base alla lunghezza della LHS (usa l'algoritmo quick sort)
sort_list(List,Sorted):-q_sort(List,[],Sorted).

q_sort([],Acc,Acc).
q_sort([Num-ID|T],Acc,Sorted):-
	pivot(Num,T,L1,L2),
	q_sort(L1,Acc,Sorted1),q_sort(L2,[Num-ID|Sorted1],Sorted).
	
pivot(_,[],[],[]).
pivot(Num,[Num2-ID2|T],[Num2-ID2|L],G):- Num >= Num2, pivot(Num,T,L,G).
pivot(Num,[Num2-ID2|T],L,[Num2-ID2|G]):- Num <  Num2, pivot(Num,T,L,G).


%ricerca del primo elemento della lista
first([ID|_], ID).

%aggiunta dell'ultimo elemento in coda
add(ID,[],[ID]).
add(ID,[Y|Tail],[Y|Tail1]):- add(ID,Tail,Tail1).

%estrazione sottolista da lista di elementi
estraiLista([],_,[]) :- !.
estraiLista([Item|_],Item,[Item]) :- !.
estraiLista([ItemX|Rest],Item,ListOut) :-
    estraiLista(Rest,Item,ListOutX),
    append([ItemX],ListOutX,ListOut),
    !.
    
%differenza tra liste
diff(L,[],L).
diff(L1,[H|L2],L3) :-
    not(member(H,L1)),
    diff(L1,L2,L3).
diff(L1,[H|L2],L3) :-
    member(H,L1),
    nth0(_,L1,H,L4),
    diff(L4,L2,L3).
             