
%calcola certezza sulla base della risposta
comandi :- 
     write('Il menù iniziale permette l\'esecuzione dei seguenti comandi: '), nl,
     write('1. carica -  carica la base di conoscenza'), nl,
     write('2. inizia analisi -  avvia il processo di diagnosi'),nl,
     write('3. riepilogo -  visualizza i fatti asseriti dal sistema'),nl,
     write('4. dettagli -  visualizza i dettagli sulle possibili anomalie riscontrabili'),nl,
     write('5. help -  visualizza i dettagli sui comandi disponibili'),nl,
     write('6. exit -  esci dal sistema'),
     nl,nl,nl,
     write('Durante l\'esecuzione è possibile selezionare le seguenti opzioni: '),nl,
     write('- Puoi chiedere chiarimenti sulla domanda sottoposta digitando: '),nl,
     write('p. - perchè. '),nl,
     write('- Puoi mostrare le soluzioni parziali digitando: '),nl,
     write('f. - fatti. '),nl,
     write('- Puoi ripartire da una domanda già sottoposta digitando: '),nl,
     write('r. - riparti. '),nl,
     write('- Puoi tornare indietro e riprendere l\'esecuzione digitando: '),nl,
     write('d. - domanda. '),
     nl,nl,nl,
     write('Al termine delle analisi è possibile eseguire i seguenti comandi: '), nl,
     write('- Digita "c." se vuoi vedere come sono arrivato a tali conclusioni'), nl,
     write('- Digita "s." per effettuare una nuova analisi'), nl,
     write('- Digita "n." per tornare al menù principale'), nl.
     
     
     
